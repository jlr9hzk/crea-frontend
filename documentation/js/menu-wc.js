'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">crea-portal documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-dc43f0dfc6ea9f1b9de84eaab96fb8e9"' : 'data-target="#xs-components-links-module-AppModule-dc43f0dfc6ea9f1b9de84eaab96fb8e9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-dc43f0dfc6ea9f1b9de84eaab96fb8e9"' :
                                            'id="xs-components-links-module-AppModule-dc43f0dfc6ea9f1b9de84eaab96fb8e9"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChatbotComponentComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChatbotComponentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VideoModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VideoModalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ComponentsModule.html" data-type="entity-link" >ComponentsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ComponentsModule-efa3784f57ffb2d5d1bbf921e1f703ae"' : 'data-target="#xs-components-links-module-ComponentsModule-efa3784f57ffb2d5d1bbf921e1f703ae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComponentsModule-efa3784f57ffb2d5d1bbf921e1f703ae"' :
                                            'id="xs-components-links-module-ComponentsModule-efa3784f57ffb2d5d1bbf921e1f703ae"' }>
                                            <li class="link">
                                                <a href="components/BalaoSugestoesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BalaoSugestoesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BtnServicosComponentComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BtnServicosComponentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChatbotComponentComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChatbotComponentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConteudoDetalhesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConteudoDetalhesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PesquisaComponentComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PesquisaComponentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ResultadoComponentComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ResultadoComponentComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VideoModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VideoModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VideosComponentComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >VideosComponentComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DetalhesPageModule.html" data-type="entity-link" >DetalhesPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DetalhesPageModule-6cd8698a1e5b1bbdba7053f5324a8bda"' : 'data-target="#xs-components-links-module-DetalhesPageModule-6cd8698a1e5b1bbdba7053f5324a8bda"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DetalhesPageModule-6cd8698a1e5b1bbdba7053f5324a8bda"' :
                                            'id="xs-components-links-module-DetalhesPageModule-6cd8698a1e5b1bbdba7053f5324a8bda"' }>
                                            <li class="link">
                                                <a href="components/DetalhesPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetalhesPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DetalhesPageRoutingModule.html" data-type="entity-link" >DetalhesPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FaqPageModule.html" data-type="entity-link" >FaqPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FaqPageModule-db176ad11bf46870ff16140e8b2d8c02"' : 'data-target="#xs-components-links-module-FaqPageModule-db176ad11bf46870ff16140e8b2d8c02"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FaqPageModule-db176ad11bf46870ff16140e8b2d8c02"' :
                                            'id="xs-components-links-module-FaqPageModule-db176ad11bf46870ff16140e8b2d8c02"' }>
                                            <li class="link">
                                                <a href="components/FaqPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FaqPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FaqPageRoutingModule.html" data-type="entity-link" >FaqPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/HomePageModule.html" data-type="entity-link" >HomePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomePageModule-77caf6b6f365ebfb61a9172788674b48"' : 'data-target="#xs-components-links-module-HomePageModule-77caf6b6f365ebfb61a9172788674b48"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomePageModule-77caf6b6f365ebfb61a9172788674b48"' :
                                            'id="xs-components-links-module-HomePageModule-77caf6b6f365ebfb61a9172788674b48"' }>
                                            <li class="link">
                                                <a href="components/HomePage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomePage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomePageRoutingModule.html" data-type="entity-link" >HomePageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PesquisaPageModule.html" data-type="entity-link" >PesquisaPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PesquisaPageModule-0795fd8fdf6d7db4025ad73ca229e043"' : 'data-target="#xs-components-links-module-PesquisaPageModule-0795fd8fdf6d7db4025ad73ca229e043"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PesquisaPageModule-0795fd8fdf6d7db4025ad73ca229e043"' :
                                            'id="xs-components-links-module-PesquisaPageModule-0795fd8fdf6d7db4025ad73ca229e043"' }>
                                            <li class="link">
                                                <a href="components/PesquisaPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PesquisaPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PesquisaPageRoutingModule.html" data-type="entity-link" >PesquisaPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ServicosPageModule.html" data-type="entity-link" >ServicosPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServicosPageModule-8692e17b91b0c0432b6e3479382559af"' : 'data-target="#xs-components-links-module-ServicosPageModule-8692e17b91b0c0432b6e3479382559af"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServicosPageModule-8692e17b91b0c0432b6e3479382559af"' :
                                            'id="xs-components-links-module-ServicosPageModule-8692e17b91b0c0432b6e3479382559af"' }>
                                            <li class="link">
                                                <a href="components/ServicosPage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServicosPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServicosPageRoutingModule.html" data-type="entity-link" >ServicosPageRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SobrePageModule.html" data-type="entity-link" >SobrePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SobrePageModule-d983b20263e21051634c94ec0a19a498"' : 'data-target="#xs-components-links-module-SobrePageModule-d983b20263e21051634c94ec0a19a498"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SobrePageModule-d983b20263e21051634c94ec0a19a498"' :
                                            'id="xs-components-links-module-SobrePageModule-d983b20263e21051634c94ec0a19a498"' }>
                                            <li class="link">
                                                <a href="components/SobrePage.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SobrePage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SobrePageRoutingModule.html" data-type="entity-link" >SobrePageRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/BalaoSugestoesComponent.html" data-type="entity-link" >BalaoSugestoesComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/BtnServicosComponentComponent.html" data-type="entity-link" >BtnServicosComponentComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ChatbotComponentComponent.html" data-type="entity-link" >ChatbotComponentComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ConteudoDetalhesComponent.html" data-type="entity-link" >ConteudoDetalhesComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/FooterComponent.html" data-type="entity-link" >FooterComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/HeaderComponent.html" data-type="entity-link" >HeaderComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/PesquisaComponentComponent.html" data-type="entity-link" >PesquisaComponentComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/PesquisaPage.html" data-type="entity-link" >PesquisaPage</a>
                            </li>
                            <li class="link">
                                <a href="components/ResultadoComponentComponent.html" data-type="entity-link" >ResultadoComponentComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/VideoModalComponent.html" data-type="entity-link" >VideoModalComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/VideosComponentComponent.html" data-type="entity-link" >VideosComponentComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link" >AppPage</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppStore.html" data-type="entity-link" >AppStore</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DetalhesService.html" data-type="entity-link" >DetalhesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PesquisaService.html" data-type="entity-link" >PesquisaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServicesService.html" data-type="entity-link" >ServicesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServicosService.html" data-type="entity-link" >ServicosService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SobrePortalService.html" data-type="entity-link" >SobrePortalService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/VideosService.html" data-type="entity-link" >VideosService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/IDetalhes.html" data-type="entity-link" >IDetalhes</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPropsDetalhes.html" data-type="entity-link" >IPropsDetalhes</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPropsServicos.html" data-type="entity-link" >IPropsServicos</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPropsSobre.html" data-type="entity-link" >IPropsSobre</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPropsTrends.html" data-type="entity-link" >IPropsTrends</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IPropsVideos.html" data-type="entity-link" >IPropsVideos</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IServicos.html" data-type="entity-link" >IServicos</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ISobre.html" data-type="entity-link" >ISobre</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ITrends.html" data-type="entity-link" >ITrends</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IVideo.html" data-type="entity-link" >IVideo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IVideo-1.html" data-type="entity-link" >IVideo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IVideos.html" data-type="entity-link" >IVideos</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PageName.html" data-type="entity-link" >PageName</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});