import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'sobre',
    loadChildren: () =>
      import('./pages/sobre/sobre.module').then((m) => m.SobrePageModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'servicos',
    loadChildren: () =>
      import('./pages/servicos/servicos.module').then(
        (m) => m.ServicosPageModule,
      ),
  },
  {
    path: 'faq',
    loadChildren: () =>
      import('./pages/faq/faq.module').then((m) => m.FaqPageModule),
  },
  {
    path: 'pesquisa',
    loadChildren: () =>
      import('./pages/pesquisa/pesquisa.module').then(
        (m) => m.PesquisaPageModule,
      ),
  },
  {
    path: 'pesquisa/:busca',
    loadChildren: () =>
      import('./pages/pesquisa/pesquisa.module').then(
        (m) => m.PesquisaPageModule,
      ),
  },
  {
    path: 'detalhes',
    loadChildren: () =>
      import('./pages/detalhes/detalhes.module').then(
        (m) => m.DetalhesPageModule,
      ),
  },
  {
    path: 'acessibilidade',
    loadChildren: () =>
      import('./pages/acessibilidade/acessibilidade.module').then(
        (m) => m.AcessibilidadePageModule,
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
