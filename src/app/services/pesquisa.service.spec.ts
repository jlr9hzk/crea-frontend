/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PesquisaService } from './pesquisa.service';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from 'ngx-webstorage/lib/core/interfaces/storageService';
import { EMPTY, Observable } from 'rxjs';
import { ITrends } from '../store/modules/trends/trends.action';
describe('PesquisaService', () => {
  let service: PesquisaService;
  const mockStore = {};
  mockStore['trends'] = { id: 1, assunto: 'a', icon: 'a.icon' } as ITrends;

  const localStorageMock: StorageService = {
    store: (key: string, value: string) => {
      mockStore[key] = `${value}`;
    },
    retrieve: (key: string): string => {
      return key in mockStore ? mockStore[key] : null;
    },
    clear: (key: string) => {
      delete mockStore[key];
    },
    getStrategyName(): string {
      return 'Local';
    },
    observe(key: string): Observable<any> {
      return EMPTY;
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{ provide: LocalStorageService, useValue: localStorageMock }],
    });
    service = TestBed.inject(PesquisaService);
    void service.search('a');
    void service.trendsSearch();
  });

  it('should be created', () => {
    void expect(service).toBeTruthy();
  });
});
