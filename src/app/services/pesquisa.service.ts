import { AppStore } from './../store/app.store';
import { ServicesService } from './services.service';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { getTrends, ITrends } from 'src/app/store/modules/trends/trends.action';
import {
  getSugestoes,
  ISugestoes,
} from '../store/modules/sugestoes/sugestoes.action';

@Injectable({
  providedIn: 'root',
})
export class PesquisaService {
  /**
   * Metodo construtor
   *
   * @param {ServicesService} service
   * @param {LocalStorageService} localSt
   * @param {AppStore} store
   */
  constructor(
    private service: ServicesService,
    private localSt: LocalStorageService,
    private store: AppStore,
  ) {}

  /**
   * Culsulta na API resultados de busca.
   *
   * @param {string} busca termo da busca
   * @returns [any] lista de resultados
   */
  async search(busca: string | number | boolean): Promise<unknown> {
    return new Promise((resolve, reject) => {
      this.service
        .getAll('/conteudo/filter/' + encodeURIComponent(busca))
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * Retorna da API as principais busca feitas no portal (trends / Assuntos mais buscados).
   */
  async trendsSearch(): Promise<void> {
    const trds: ITrends[] = this.localSt.retrieve('trends') as ITrends[];
    if (trds) {
      this.store.dispatch(getTrends({ trends: trds }));
    }

    /**
     * Trends de Pesquisa, (Assuntos mais buscados)
     */
    await this.service
      .getAll('/trend/getMaisBuscados')
      .then((data: ITrends[]) => {
        if (trds) {
          if (trds.length !== data.length) {
            this.localSt.store('trends', data);
            this.store.dispatch(getTrends({ trends: data }));
          }
        } else {
          this.localSt.store('trends', data);
          this.store.dispatch(getTrends({ trends: data }));
        }
      })
      .catch(() => {
        //
      }); //- - - FIM trends Pesquisa
  }

  async SuggestionSearch(): Promise<void> {
    const sgts: ISugestoes[] = this.localSt.retrieve(
      'sugestoes',
    ) as ISugestoes[];

    if (sgts) {
      this.store.dispatch(getSugestoes({ sugestoes: sgts }));
    }

    /**
     * Assuntos mais buscados
     */

    await this.service
      .getAll('/trend/getSugestao')
      .then((data: ISugestoes[]) => {
        if (sgts) {
          if (sgts.length !== data.length) {
            this.localSt.store('sugestoes', data);
            this.store.dispatch(getSugestoes({ sugestoes: data }));
          }
        } else {
          this.localSt.store('sugestoes', data);
          this.store.dispatch(getSugestoes({ sugestoes: data }));
        }
      })
      .catch(() => {
        //
      });
  }
}
