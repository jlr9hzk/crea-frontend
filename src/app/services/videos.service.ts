import { ServicesService } from './services.service';
import { AppStore } from './../store/app.store';
import { LocalStorageService } from 'ngx-webstorage';
import { Injectable } from '@angular/core';
import { getVideos, IVideos } from '../store/modules/videos/videos.action';

@Injectable({
  providedIn: 'root',
})
export class VideosService {
  /**
   * Metodo construtor
   *
   * @param {LocalStorageService} localSt
   * @param {AppStore} store
   * @param {ServicesService} service
   */
  constructor(
    private localSt: LocalStorageService,
    private store: AppStore,
    private service: ServicesService,
  ) {}

  /**
   * Retorna os videos da API, passando a URL especifica do tipo de video.
   * Mas antes do resultado da API, já carrega os videos em cache.
   * O resultado é armazenado no local store e passado para o redux.
   * @example
   * getVideosAllUrl("video/getAll")
   *
   * @param {string} url complemento da API, link.
   */
  async getVideosAllUrl(url: string): Promise<void> {
    const vds: IVideos[] = this.localSt.retrieve('videos') as IVideos[];
    if (vds) {
      this.store.dispatch(getVideos({ videos: vds }));
    }
    await this.service
      .getAll(url)
      .then((data: IVideos[]) => {
        if (vds) {
          if (vds.length !== data.length) {
            this.store.dispatch(getVideos({ videos: data }));
            this.localSt.store('videos', data);
          }
        } else {
          this.store.dispatch(getVideos({ videos: data }));
          this.localSt.store('videos', data);
        }
      })
      .catch(() => {
        //
      });
  }

  /**
   * Retorna os Videos da Pesquisa
   *
   * @example
   * getVideosFilter("video/filter/{term}")
   *
   * @param {string} url complemento da API, link. passando o parametro de busca ao final.
   */
  async getVideosFilter(filter: string | number | boolean): Promise<unknown> {
    return new Promise((resolve, reject) => {
      this.service
        .getAll('/video/filter/' + encodeURIComponent(filter))
        .then((data: IVideos[]) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
