import { getSobre, ISobre } from './../store/modules/sobre/sobre.action';
import { AppStore } from './../store/app.store';
import { LocalStorageService } from 'ngx-webstorage';
import { ServicesService } from './services.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SobrePortalService {
  /**
   * Metodo construtor
   *
   * @param {ServicesService} service
   * @param {LocalStorageService} localSt
   * @param {AppStore} store
   */
  constructor(
    private service: ServicesService,
    private localSt: LocalStorageService,
    private store: AppStore,
  ) {}

  /**
   * Retorna da API a descricão do portal e o título.
   * Mas antes do resultado da API, já carrega a descricao em cache.
   * O resultado é armazenado no local store e passado para o redux.
   */
  async getSobrePortal(): Promise<void> {
    const port: ISobre = (await this.localSt.retrieve('portal')) as ISobre;
    if (port) {
      this.store.dispatch(getSobre({ sobre: port }));
    }

    await this.service
      .getAll('/sobre/getAll')
      .then((data: ISobre[]) => {
        if (data.length > 0) {
          this.localSt.store('portal', data[0]);
          this.store.dispatch(getSobre({ sobre: data[0] }));
        }
      })
      .catch(() => {
        //
      }); //- - - FIM trends
  }
}
