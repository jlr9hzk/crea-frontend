import { ServicesService } from './services.service';
import { LocalStorageService } from 'ngx-webstorage';
import { Injectable } from '@angular/core';
import { AppStore } from '../store/app.store';
import {
  getServicos,
  IServicos,
} from 'src/app/store/modules/servicos/servicos.action';

@Injectable({
  providedIn: 'root',
})
export class ServicosService {
  /**
   *  Metodo construtor
   *
   * @param {LocalStorageService} localSt
   * @param {AppStore} store
   * @param {ServicesService} service
   */
  constructor(
    private localSt: LocalStorageService,
    private store: AppStore,
    private service: ServicesService,
  ) {}

  /**
   * Retorna da API a os servicos disponiveis no portal.
   * Mas antes do resultado da API, já carrega os servicos em cache.
   * O resultado é armazenado no local store e passado para o redux.
   */
  async getServicosAll(): Promise<void> {
    const servicos: IServicos[] = this.localSt.retrieve(
      'servicos',
    ) as IServicos[];
    if (servicos) {
      this.store.dispatch(getServicos({ servicos: servicos }));
    }

    await this.service
      .getAll('/conteudo/getServicos')
      .then((data: IServicos[]): void => {
        if (servicos) {
          if (servicos.length !== data.length) {
            this.store.dispatch(getServicos({ servicos: data }));
            this.localSt.store('servicos', data);
          }
        } else {
          this.store.dispatch(getServicos({ servicos: data }));
          this.localSt.store('servicos', data);
        }
      })
      .catch(() => {
        //
      });
  }
}
