import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ServicesService } from './services.service';
import { LocalStorageService } from 'ngx-webstorage';
describe('ServicesService', () => {
  let service: ServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [LocalStorageService],
    });
    service = TestBed.inject(ServicesService);
    void service.getAll('/servico/getAll');
    service.getEndpoint('/servico/getAll');
  });

  it('should be created', () => {
    void expect(service).toBeTruthy();
  });
});
