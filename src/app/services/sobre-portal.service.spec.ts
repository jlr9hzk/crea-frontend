/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { TestBed, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SobrePortalService } from './sobre-portal.service';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from 'ngx-webstorage/lib/core/interfaces/storageService';
import { EMPTY, Observable } from 'rxjs';
import { ISobre } from '../store/modules/sobre/sobre.action';
describe('SobrePortalService', () => {
  let service: SobrePortalService;
  const mockStore = {};
  mockStore['portal'] = { titulo: 'teste', descricao: 'teste' } as ISobre;
  const localStorageMock: StorageService = {
    store: (key: string, value: string) => {
      mockStore[key] = `${value}`;
    },
    retrieve: (key: string): string => {
      return key in mockStore ? mockStore[key] : null;
    },
    clear: (key: string) => {
      delete mockStore[key];
    },
    getStrategyName(): string {
      return 'Local';
    },
    observe(key: string): Observable<any> {
      return EMPTY;
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{ provide: LocalStorageService, useValue: localStorageMock }],
    });
    service = TestBed.inject(SobrePortalService);
    void service.getSobrePortal();
  });

  it('should be created', () => {
    void expect(service).toBeTruthy();
  });
});
