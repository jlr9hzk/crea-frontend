import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { DetalhesService } from './detalhes.service';

describe('DetalhesService', () => {
  let service: DetalhesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
    });
    service = TestBed.inject(DetalhesService);
    void service.getConteudoId(1);
  });

  it('should be created', () => {
    void expect(service).toBeTruthy();
  });
});
