/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ServicosService } from './servicos.service';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from 'ngx-webstorage/lib/core/interfaces/storageService';
import { EMPTY, Observable } from 'rxjs';
import { IServicos } from '../store/modules/servicos/servicos.action';
describe('ServicosService', () => {
  let service: ServicosService;
  const mockStore = {};
  mockStore['servicos'] = { id: 1, titulo: 'a', icon: 'a.icon' } as IServicos;
  const localStorageMock: StorageService = {
    store: (key: string, value: string) => {
      mockStore[key] = `${value}`;
    },
    retrieve: (key: string): string => {
      return key in mockStore ? mockStore[key] : null;
    },
    clear: (key: string) => {
      delete mockStore[key];
    },
    getStrategyName(): string {
      return 'Local';
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    observe(key: string): Observable<any> {
      return EMPTY;
    },
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{ provide: LocalStorageService, useValue: localStorageMock }],
    });
    service = TestBed.inject(ServicosService);
    void service.getServicosAll();
  });

  it('should be created', () => {
    void expect(service).toBeTruthy();
  });
});
