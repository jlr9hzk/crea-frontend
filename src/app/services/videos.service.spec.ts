/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { VideosService } from './videos.service';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from 'ngx-webstorage/lib/core/interfaces/storageService';
import { EMPTY, Observable } from 'rxjs';
import { IVideo } from '../components/video-modal/video-modal.component';

describe('VideosService', () => {
  let service: VideosService;
  const mockStore = {};
  mockStore['videos'] = {
    id: 1,
    titulo: 'a',
    urlVideo: 'https://www.youtube.com/watch?v=hPqbrYLXWaE',
    descricao: 'a',
    safeUrl: '',
  } as IVideo;
  const localStorageMock: StorageService = {
    store: (key: string, value: string) => {
      mockStore[key] = `${value}`;
    },
    retrieve: (key: string): string => {
      return key in mockStore ? mockStore[key] : null;
    },
    clear: (key: string) => {
      delete mockStore[key];
    },
    getStrategyName(): string {
      return 'Local';
    },
    observe(key: string): Observable<any> {
      return EMPTY;
    },
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{ provide: LocalStorageService, useValue: localStorageMock }],
    });
    service = TestBed.inject(VideosService);
    void service.getVideosAllUrl('/api/video/getAll');
    void service.getVideosFilter('ART');
  });

  it('should be created', () => {
    void expect(service).toBeTruthy();
  });
});
