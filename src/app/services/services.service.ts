import { Injectable } from '@angular/core';
import { URL_SERVE } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ServicesService {
  /**
   * Metodo construtor
   *
   * @param {HttpClient} http
   */
  constructor(private http: HttpClient) {}

  /**
   * Junta o subdiretório ao dominio
   * @param {string} url
   * @returns {string} url completa
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async getAll(url: string): Promise<any> {
    return await this.http.get(this.getEndpoint(url)).toPromise();
  }

  /**
   * Endpoint URL
   *
   * @param url
   * @returns {string} url completa
   */
  getEndpoint(url: string): string {
    return `${URL_SERVE}/api${url}`;
  }
}
