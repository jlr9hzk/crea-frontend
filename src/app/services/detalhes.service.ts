import { ServicesService } from './services.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DetalhesService {
  /**
   * Metodo construtor
   * @param {ServicesService} service
   */
  constructor(private service: ServicesService) {}

  /**
   * Retorna da API os detalhes de pesquisa selecionada, passando o ID.
   * @param {number} id id da pesquise selecionada.
   * @returns [any]
   */
  async getConteudoId(id: number): Promise<unknown> {
    return new Promise((resolve) => {
      this.service
        .getAll(`/conteudo/${id}`)
        .then((data) => {
          resolve(data);
        })
        .catch(() => {
          //
        });
    });
  }

  async getPostId(id: number): Promise<unknown> {
    return new Promise((resolve) => {
      this.service
        .getAll(`/conteudo/${id}`)
        .then((data) => {
          resolve(data);
        })
        .catch(() => {
          //
        });
    });
  }
}
