/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [AppComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [HttpClientTestingModule, RouterTestingModule],
      }).compileComponents();
    }),
  );

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    component.isChrome = true;
    component.isEdge = true;
    component.isIE = true;
    component.isOpera = true;
    component.isMacOs = true;
    component.isSafari = true;
    component.os;
    component.browser;
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    void expect(app).toBeTruthy();
  });
  // TODO: add more tests!
});
