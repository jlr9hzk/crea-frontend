import { SafeResourceUrl } from '@angular/platform-browser';
import { createAction } from '@reduxjs/toolkit';

/**
 * Actions que lidam com o estado global 'videos'
 */
enum ActionTypes {
  listVideos = 'LIST_VIDEOS',
  openPlayer = 'OPEN_PLAYER',
  closePlayer = 'CLOSE_PLAYER',
}

/**
 * Interface da variável global "videos".
 * */
export interface IVideos {
  id: number;
  titulo: string;
  urlVideo: string;
  descricao: string;
  safeUrl?: SafeResourceUrl;
}

/**
 * Interface do campo payload da action
 *  */
export interface IPropsVideos {
  videos: IVideos[];
  video?: IVideos;
}

/**
 * Função de utilidade que define o tipo do payload da action.
 * O tipo passado pelo Generic também será o tipo do retorno.
 * @typeParam T nome da 'tipagem'
 *
 */
function withPayloadType<T>() {
  return (videos: T) => ({ payload: videos });
}

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "videos".
 *  Quando disparada na aplicação ela deverá passar os dados vindos da api pelo campo payload
 *  e armazená-los no estado global.
 */

export const getVideos = createAction(
  ActionTypes.listVideos,
  withPayloadType<IPropsVideos>(),
);

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "video".
 *  Quando disparada na aplicação ela deverá passar os dados do vídeo para
 *  ser exibido no player e armazená-lo no estado global.
 *  Com a variável global "video" instanciada o player de vídeo será exibido na aplicação.
 */

export const openVideoPlayer = createAction(
  ActionTypes.openPlayer,
  withPayloadType<Omit<IPropsVideos, 'videos'>>(),
);

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "video".
 *  Quando disparada na aplicação ela deverá limpar a variável global "vídeo"
 *  definindo-a como undefined, fazendo com que o player deixe de ser exibido na aplicação.
 */

export const closeVideoPlayer = createAction(ActionTypes.closePlayer);
