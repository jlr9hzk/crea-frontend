import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import {
  getVideos,
  openVideoPlayer,
  IPropsVideos,
  IVideos,
  closeVideoPlayer,
} from './videos.action';

/**
 * Valor inicial da variável global "detalhes"
 */
export const initialStateVideos: IPropsVideos = {
  videos: [],
};

/**
 *  Reducer criado através do método criador de reducers do redux toolkit
 *  para lidar com as variáveis globais "videos" e "video".
 */
export const videoReducer = createReducer(initialStateVideos, (builder) => {
  builder.addCase(getVideos, (state, action: PayloadAction<IPropsVideos>) => {
    state = {
      ...state,
      videos: action.payload.videos,
    };

    return state;
  });

  builder.addCase(
    openVideoPlayer,
    (state, action: PayloadAction<Omit<IPropsVideos, 'videos'>>) => {
      state = {
        ...state,
        video: action.payload.video,
      };

      return state;
    },
  );

  builder.addCase(closeVideoPlayer, (state) => {
    state = {
      ...state,
      video: undefined as IVideos,
    };

    return state;
  });
});
