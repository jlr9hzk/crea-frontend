import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { getServicos, IPropsServicos } from './servicos.action';

/**
 * Valor inicial da variável global "servicos"
 */
export const initialStateVideos: IPropsServicos = {
  servicos: [],
};

/**
 *  Reducer criado através do método criador de reducers do redux toolkit
 *  para lidar com a variável global "servicos".
 */
export const servicoReducer = createReducer(initialStateVideos, (builder) => {
  builder.addCase(
    getServicos,
    (state, action: PayloadAction<IPropsServicos>) => {
      state = {
        ...state,
        servicos: action.payload.servicos,
      };

      return state;
    },
  );
});
