import { createAction } from '@reduxjs/toolkit';

/**
 * Nome das ações que lidam com o estado global 'servicos'
 */
enum ActionTypes {
  listServices = 'LISTAR_SERVICOS',
}

/**
 * Interface da variável global "servicos".
 * */

export interface IServicos {
  id: number;
  titulo: string;
  icon: string;
}

/**
 * Interface do campo payload da action
 *  */
export interface IPropsServicos {
  servicos: IServicos[];
}

/**
 * Valor inicial da variável global "detalhes"
 */
export const appInitialStateServicos: IPropsServicos = {
  servicos: [],
};

/**
 * Função de utilidade que define o tipo do payload da action.
 * O tipo passado pelo Generic também será o tipo do retorno.
 * @typeParam T nome da 'tipagem'
 *
 */
function withPayloadType<T>() {
  return (servicos: T) => ({ payload: servicos });
}

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "servicos".
 *  Quando disparada na aplicação ela deverá passar os dados vindos da api pelo campo payload
 *  e armazená-los no estado global.
 */

export const getServicos = createAction(
  ActionTypes.listServices,
  withPayloadType<IPropsServicos>(),
);
