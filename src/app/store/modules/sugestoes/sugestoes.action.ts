import { createAction } from '@reduxjs/toolkit';

/**
 * Nome das ações que lidam com o estado global 'sugestoes'
 */
enum ActionTypes {
  listSugestoes = 'LISTAR_SUGESTOES',
}

/**
 * Interface da variável global "sugestoes".
 * */
export interface ISugestoes {
  id: number;
  nome: string;
  icon: string;
}

/**
 * Interface do campo payload da action
 *  */
export interface IPropsSugestoes {
  sugestoes: ISugestoes[];
}

/**
 * Valor inicial da variável global "sugestoes"
 */
export const appInitialStateSugestoes: IPropsSugestoes = {
  sugestoes: [],
};

/**
 * Função de utilidade que define o tipo do payload da action.
 * O tipo passado pelo Generic também será o tipo do retorno.
 * @typeParam T nome da 'tipagem'
 *
 */
function withPayloadType<T>() {
  return (sugestoes: T) => ({ payload: sugestoes });
}

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "sugestoes".
 *  Quando disparada na aplicação ela deverá passar os dados vindos da api pelo campo payload
 *  e armazená-los no estado global.
 */

export const getSugestoes = createAction(
  ActionTypes.listSugestoes,
  withPayloadType<IPropsSugestoes>(),
);
