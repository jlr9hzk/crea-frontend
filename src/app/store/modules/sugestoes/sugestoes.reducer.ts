import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { getSugestoes, IPropsSugestoes } from '../sugestoes/sugestoes.action';

/**
 * Valor inicial da variável global "sugestoes"
 */
export const initialStateSugestoes: IPropsSugestoes = {
  sugestoes: [],
};

/**
 *  Reducer criado através do método criador de reducers do redux toolkit
 *  para lidar com a variável global "sugestoes".
 */

export const sugestoesReducer = createReducer(
  initialStateSugestoes,
  (builder) => {
    builder.addCase(
      getSugestoes,
      (state, action: PayloadAction<IPropsSugestoes>) => {
        state = {
          ...state,
          sugestoes: action.payload.sugestoes,
        };

        return state;
      },
    );
  },
);
