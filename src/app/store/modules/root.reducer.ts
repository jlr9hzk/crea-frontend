import { detalhesReducer } from './detalhes/detalhes.reducer';
import { combineReducers } from 'redux';

import { videoReducer } from './videos/videos.reducer';
import { servicoReducer } from './servicos/servicos.reducer';
import { sobreReducer } from './sobre/sobre.reducer';
import { trendsReducer } from './trends/trends.reducer';
import { sugestoesReducer } from './sugestoes/sugestoes.reducer';

/**
 * Cria um uníco reducer raiz para gerenciamento de estado da aplicação,
 * combinando todos os reducers criados.
 */
const rootReducer = combineReducers({
  videoReducer,
  servicoReducer,
  sobreReducer,
  trendsReducer,
  detalhesReducer,
  sugestoesReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
