import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { getSobre, IPropsSobre } from '../sobre/sobre.action';

/**
 * Valor inicial da variável global "sobre"
 */
export const initialStateSobre: IPropsSobre = {
  sobre: {
    titulo: 'Portal de Serviços CREA-SP',
    descricao: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Morbi non arcu risus quis varius quam quisque id diam. Augue mauris augue neque gravida in. Habitant morbi tristique senectus et netus et malesuada. Viverra nibh cras pulvinar mattis nunc sed blandit libero volutpat. Quam lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Leo duis ut diam quam nulla porttitor. Pretium fusce id velit ut tortor pretium viverra suspendisse potenti. Potenti nullam ac tortor vitae purus faucibus ornare.

    Neque gravida in fermentum et sollicitudin ac. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Venenatis urna cursus eget nunc scelerisque viverra mauris in. Quis risus sed vulputate odio ut enim blandit volutpat maecenas. Nec ullamcorper sit amet risus. Sem viverra aliquet eget sit amet tellus cras adipiscing. Sed arcu non odio euismod. Diam maecenas ultricies mi eget. Diam sit amet nisl suscipit adipiscing bibendum est. Urna porttitor rhoncus dolor purus non enim. Id nibh tortor id aliquet lectus. Mi sit amet mauris commodo quis imperdiet. Nulla pharetra diam sit amet nisl suscipit adipiscing bibendum est.

    Donec ac odio tempor orci dapibus ultrices in. Eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis. Vitae tempus quam pellentesque nec nam aliquam. At imperdiet dui accumsan sit amet nulla facilisi morbi tempus.

    Luctus accumsan tortor posuere ac ut consequat. Viverra tellus in hac habitasse. Diam maecenas sed enim ut sem.`,
  },
};

/**
 *  Reducer criado através do método criador de reducers do redux toolkit
 *  para lidar com a variável global "sobre".
 */
export const sobreReducer = createReducer(initialStateSobre, (builder) => {
  builder.addCase(getSobre, (state, action: PayloadAction<IPropsSobre>) => {
    state = {
      ...state,
      sobre: action.payload.sobre,
    };

    return state;
  });
});
