import { createAction } from '@reduxjs/toolkit';

/**
 * Nome das ações que lidam com o estado global 'sobre'
 */
enum ActionTypes {
  listSobre = 'LISTAR_SOBRE',
}

/**
 * Interface da variável global "sobre".
 * */
export interface ISobre {
  titulo: string;
  descricao: string;
}

/**
 * Interface do campo payload da action
 *  */
export interface IPropsSobre {
  sobre: ISobre;
}

/**
 * Função de utilidade que define o tipo do payload da action.
 * O tipo passado pelo Generic também será o tipo do retorno.
 * @typeParam T nome da 'tipagem'
 *
 */

function withPayloadType<T>() {
  return (sobre: T) => ({ payload: sobre });
}

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "sobre".
 *  Quando disparada na aplicação ela deverá passar os dados vindos da api pelo campo payload
 *  e armazená-los no estado global.
 */

export const getSobre = createAction(
  ActionTypes.listSobre,
  withPayloadType<IPropsSobre>(),
);
