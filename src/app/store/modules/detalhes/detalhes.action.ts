import { createAction } from '@reduxjs/toolkit';

/**
 * Nome das ações que lidam com o estado global 'detalhes'
 */
enum ActionTypes {
  detalhesConteudo = 'CONTEUDO_DETALHES',
}

/**
 * Interface da variável global "detalhes".
 * */

export interface IDetalhes {
  id: number;
  titulo: string;
  descricao: string;
}

/**
 * Interface do campo payload da action
 *  */
export interface IPropsDetalhes {
  detalhes: IDetalhes[];
}

/**
 * Valor inicial da variável global "detalhes"
 */
export const appInitialStateDetalhes: IPropsDetalhes = {
  detalhes: [],
};

/**
 * Função de utilidade que define o tipo do payload da action.
 * O tipo passado pelo Generic também será o tipo do retorno.
 * @typeParam T nome da 'tipagem'
 *
 */
function withPayloadType<T>() {
  return (detalhes: T) => ({ payload: detalhes });
}

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "detalhes".
 *  Quando disparada na aplicação ela deverá passar os dados vindos da api pelo campo payload
 *  e armazená-los no estado global.
 */

export const getDetalhes = createAction(
  ActionTypes.detalhesConteudo,
  withPayloadType<IPropsDetalhes>(),
);
