import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { getDetalhes, IPropsDetalhes } from './detalhes.action';

/**
 * Valor inicial da variável global "detalhes"
 */
export const initialStateDetalhes: IPropsDetalhes = {
  detalhes: [],
};

/**
 *  Reducer criado através do método criador de reducers do redux toolkit
 *  para lidar com a variável global "detalhes".
 */
export const detalhesReducer = createReducer(
  initialStateDetalhes,
  (builder) => {
    builder.addCase(
      getDetalhes,
      (state, action: PayloadAction<IPropsDetalhes>) => {
        state = {
          ...state,
          detalhes: action.payload.detalhes,
        };

        return state;
      },
    );
  },
);
