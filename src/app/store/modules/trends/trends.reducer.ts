import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { getTrends, IPropsTrends } from '../trends/trends.action';

/**
 * Valor inicial da variável global "trends"
 */
export const initialStateVideos: IPropsTrends = {
  trends: [],
};

/**
 *  Reducer criado através do método criador de reducers do redux toolkit
 *  para lidar com a variável global "trnds".
 */

export const trendsReducer = createReducer(initialStateVideos, (builder) => {
  builder.addCase(getTrends, (state, action: PayloadAction<IPropsTrends>) => {
    state = {
      ...state,
      trends: action.payload.trends,
    };

    return state;
  });
});
