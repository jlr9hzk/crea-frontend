import { createAction } from '@reduxjs/toolkit';

/**
 * Nome das ações que lidam com o estado global 'trends'
 */
enum ActionTypes {
  listTrends = 'LISTAR_TRENDS',
}

/**
 * Interface da variável global "trends".
 * */
export interface ITrends {
  id: number;
  assunto: string;
  icon: string;
}

/**
 * Interface do campo payload da action
 *  */
export interface IPropsTrends {
  trends: ITrends[];
}

/**
 * Valor inicial da variável global "trends"
 */
export const appInitialStateTrends: IPropsTrends = {
  trends: [],
};

/**
 * Função de utilidade que define o tipo do payload da action.
 * O tipo passado pelo Generic também será o tipo do retorno.
 * @typeParam T nome da 'tipagem'
 *
 */
function withPayloadType<T>() {
  return (trends: T) => ({ payload: trends });
}

/**
 *  Action criada através do método criador de ações do redux toolkit
 *  para lidar com a variável global "trends".
 *  Quando disparada na aplicação ela deverá passar os dados vindos da api pelo campo payload
 *  e armazená-los no estado global.
 */

export const getTrends = createAction(
  ActionTypes.listTrends,
  withPayloadType<IPropsTrends>(),
);
