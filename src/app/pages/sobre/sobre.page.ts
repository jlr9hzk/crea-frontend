/* eslint-disable @angular-eslint/component-class-suffix */
import { SobrePortalService } from './../../services/sobre-portal.service';
import { AppStore } from '../../store/app.store';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { handleTitleColor } from 'src/app/utils/handleTitleColor';
import { PesquisaService } from 'src/app/services/pesquisa.service';
import { DetalhesService } from 'src/app/services/detalhes.service';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.page.html',
  styleUrls: ['./sobre.page.scss'],
})
export class SobrePage implements OnInit {
  /**
   * Método construtor
   *
   * @param {AppStore} store
   * @param {SobrePortalService} sobrePortalService
   */
  constructor(
    private store: AppStore,
    private sobrePortalService: SobrePortalService,
    private pesquisaService: PesquisaService,
    private detalhesService: DetalhesService,
  ) {}

  aboutData = {};
  aboutDetails: {
    descricao?: string;
  } = {};

  /**
   * Redux de videos
   * @param {Observable} portal$
   */
  portal$ = this.store
    .select((state) => state.sobreReducer)
    .pipe(map((e) => e.sobre));

  /**
   * Redux Trends de busca
   * @param {Observable} trends$
   */
  trends$ = this.store
    .select((state) => state.trendsReducer)
    .pipe(map((e) => e.trends));

  /**
   * OnInit
   * Executa as devidas funçoes toda vez em que a página é carregada.
   */
  async ngOnInit(): Promise<void> {
    // await this.sobrePortalService.getSobrePortal();
    await this.getAboutData();
  }

  /**
   * Executa toda vez em que a pagina Home é renderizada.
   */
  ionViewDidEnter(): void {
    handleTitleColor({ componentName: 'app-sobre' });
  }

  /**
   * Obtem os resultados da busca por 'acessibilidade'
   *
   */

  async getAboutData(): Promise<void> {
    this.aboutData = await this.pesquisaService.search('portal-sobre');

    const postId = Number((this.aboutData[0] as { id: number })?.id);

    this.aboutDetails = await this.getAboutDetails(postId);
  }

  /**
   * Obtem os detalhes da busca por 'acessibilidade'
   * @param {number} postId
   *
   */

  async getAboutDetails(postId: number): Promise<{ descricao?: string }> {
    this.aboutDetails = await this.detalhesService.getPostId(postId);
    return { descricao: this.aboutDetails.descricao };
  }

  /**
   * Executa toda vez em que o Scroll da pagina Home é acionado.
   */
  onScroll(): void {
    handleTitleColor({ componentName: 'app-sobre' });
  }

  /**
   * Executa toda vez em que o Resize da pagina Home é acionado.
   */
  onResize(): void {
    handleTitleColor({ componentName: 'app-sobre' });
  }
}
