/* eslint-disable @angular-eslint/component-class-suffix */
import { VideosService } from './../../services/videos.service';
import { ServicosService } from './../../services/servicos.service';
import { Component, OnInit } from '@angular/core';
import { AppStore } from '../../store/app.store';
import { map } from 'rxjs/operators';
import { handleTitleColor } from 'src/app/utils/handleTitleColor';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-servicos',
  templateUrl: './servicos.page.html',
  styleUrls: ['./servicos.page.scss'],
})
export class ServicosPage implements OnInit {
  /**
   * Redux de videos
   * @param {Observable} videos$
   */
  videos$ = this.store
    .select((state) => state.videoReducer)
    .pipe(map((e) => e.videos));

  /**
   * Redux de Servicos
   * @param {Observable} servicos$
   */
  servicos$ = this.store
    .select((state) => state.servicoReducer)
    .pipe(map((e) => e.servicos));

  /**
   * Método construtor
   *
   * @param store
   * @param servicosService
   * @param videosService
   */
  constructor(
    private store: AppStore,
    private servicosService: ServicosService,
    private videosService: VideosService,
    private navCtrl: NavController,
  ) {}

  /**
   * Redux Trends de busca
   * @param {Observable} trends$
   */
  trends$ = this.store
    .select((state) => state.trendsReducer)
    .pipe(map((e) => e.trends));

  /**
   * OnInit
   * Executa as devidas funçoes toda vez em que a página é carregada.
   */
  async ngOnInit(): Promise<void> {
    await this.servicosService.getServicosAll();

    await this.videosService.getVideosAllUrl('/video/getPortal');
  }

  /**
   * Executa toda vez em que a pagina Home é renderizada.
   */
  ionViewDidEnter(): void {
    handleTitleColor({ componentName: 'app-servicos' });
  }

  /**
   * Executa toda vez em que o Scroll da pagina Home é acionado.
   */
  onScroll(): void {
    handleTitleColor({ componentName: 'app-servicos' });
  }

  /**
   * Executa toda vez em que o Resize da pagina Home é acionado.
   */
  onResize(): void {
    handleTitleColor({ componentName: 'app-servicos' });
  }

  /**
   * Chama pagina de pesquisa passando por parametro o servico clicado.
   *
   * @param {string} query
   */
  search(query: string): void {
    if (query?.toLowerCase() === 'cursos e eventos') {
      window.open('https://www.creasp.org.br/curso/', '_blank');
    } else {
      void this.navCtrl.navigateForward(`/pesquisa?busca=${encodeURI(query)}`);
    }
  }
}
