/* eslint-disable @angular-eslint/component-class-suffix */
/* eslint-disable @typescript-eslint/no-misused-promises */
import { DetalhesService } from './../../services/detalhes.service';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { map } from 'rxjs/operators';
import { AppStore } from 'src/app/store/app.store';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})
export class DetalhesPage {
  /**
   * JSON de detalhes do conteudo
   */

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  detalhes: any;

  /**
   * Metodo Construtor
   *
   * @param {ActivatedRoute} route
   * @param {DetalhesService} detalhesService
   * @param {Location} _location
   */
  constructor(
    private route: ActivatedRoute,
    private detalhesService: DetalhesService,
    private _location: Location,
    private store: AppStore,
  ) {}

  trends$ = this.store
    .select((state) => state.trendsReducer)
    .pipe(map((e) => e.trends));

  /**
   * Obtem os detalhes do conteudo buscado
   */
  ionViewDidEnter(): void {
    this.route.queryParams.subscribe(async (params) => {
      if (params.id) {
        this.detalhes = await this.detalhesService.getConteudoId(params.id);
        if (!this.detalhes) {
          this._location.back();
          alert('404 \n Pagina não encontrada!');
        }
      } else {
        this._location.back();
        alert('Pagina não encontrada!');
      }
    });
  }
}
