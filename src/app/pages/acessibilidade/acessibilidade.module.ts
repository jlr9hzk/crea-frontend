import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcessibilidadePageRoutingModule } from './acessibilidade-routing.module';

import { AcessibilidadePage } from './acessibilidade.page';

import { SafeHtmlPipeModule } from '../../safe-html.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcessibilidadePageRoutingModule,
    ComponentsModule,
    SafeHtmlPipeModule,
  ],
  declarations: [AcessibilidadePage],
})
export class AcessibilidadePageModule {}
