/* eslint-disable prefer-const */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { LocalStorageService } from 'ngx-webstorage';
import { SafeHtmlPipe } from 'src/app/safe-html.pipe';
import * as handle from 'src/app/utils/handleTitleColor';

import { AcessibilidadePage } from './acessibilidade.page';

describe('AcessibilidadePage', () => {
  let component: AcessibilidadePage;
  let fixture: ComponentFixture<AcessibilidadePage>;

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [AcessibilidadePage, SafeHtmlPipe],
        imports: [IonicModule.forRoot(), HttpClientTestingModule],
        providers: [LocalStorageService],
      }).compileComponents();

      fixture = TestBed.createComponent(AcessibilidadePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should handle sections title color on render view', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.ionViewDidEnter();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-acessibilidade',
    });
  }));

  it('should handle sections title color on scroll', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.onScroll();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-acessibilidade',
    });
  }));

  it('should handle sections title color on resize', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.onResize();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-acessibilidade',
    });
  }));
});
