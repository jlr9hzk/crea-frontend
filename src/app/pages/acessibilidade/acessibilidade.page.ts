/* eslint-disable @angular-eslint/component-class-suffix */
import { AppStore } from '../../store/app.store';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { handleTitleColor } from 'src/app/utils/handleTitleColor';
import { PesquisaService } from 'src/app/services/pesquisa.service';
import { DetalhesService } from 'src/app/services/detalhes.service';

@Component({
  selector: 'app-acessibilidade',
  templateUrl: './acessibilidade.page.html',
  styleUrls: ['./acessibilidade.page.scss'],
})
export class AcessibilidadePage implements OnInit {
  /**
   * Método construtor
   *
   * @param {AppStore} store
   */
  constructor(
    private store: AppStore,
    private pesquisaService: PesquisaService,
    private detalhesService: DetalhesService,
  ) {}

  accessibilityData = {};
  accessibilityDetails: {
    descricao?: string;
  } = {};

  /**
   * Redux de videos
   * @param {Observable} portal$
   */
  portal$ = this.store
    .select((state) => state.sobreReducer)
    .pipe(map((e) => e.sobre));

  /**
   * Redux Trends de busca
   * @param {Observable} trends$
   */
  trends$ = this.store
    .select((state) => state.trendsReducer)
    .pipe(map((e) => e.trends));

  /**
   * Executa toda vez em que a pagina Home é renderizada.
   */
  ionViewDidEnter(): void {
    handleTitleColor({ componentName: 'app-acessibilidade' });
  }

  /**
   * Obtem os resultados da busca por 'acessibilidade'
   *
   */

  async getAccessibilityData(): Promise<void> {
    this.accessibilityData = await this.pesquisaService.search(
      'acessibilidade',
    );

    const postId = Number((this.accessibilityData[0] as { id: number })?.id);

    this.accessibilityDetails = await this.getAccessibilityDetails(postId);
  }

  /**
   * Obtem os detalhes da busca por 'acessibilidade'
   * @param {number} postId
   *
   */

  async getAccessibilityDetails(
    postId: number,
  ): Promise<{ descricao?: string }> {
    this.accessibilityDetails = await this.detalhesService.getPostId(postId);
    return { descricao: this.accessibilityDetails.descricao };
  }

  ngOnInit(): void {
    void this.getAccessibilityData();
  }

  /**
   * Executa toda vez em que o Scroll da pagina Home é acionado.
   */
  onScroll(): void {
    handleTitleColor({ componentName: 'app-acessibilidade' });
  }

  /**
   * Executa toda vez em que o Resize da pagina Home é acionado.
   */
  onResize(): void {
    handleTitleColor({ componentName: 'app-acessibilidade' });
  }
}
