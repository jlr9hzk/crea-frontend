/* eslint-disable @typescript-eslint/no-floating-promises */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PesquisaPage } from './pesquisa.page';
import { LocalStorageService } from 'ngx-webstorage';
import * as handle from 'src/app/utils/handleTitleColor';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('PesquisaPage', () => {
  let component: PesquisaPage;
  let fixture: ComponentFixture<PesquisaPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PesquisaPage],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        providers: [
          LocalStorageService,
          {
            provide: ActivatedRoute,
            useValue: {
              queryParams: of({
                busca: 'art',
              }),
            },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(PesquisaPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle sections title color on render view', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.ionViewDidEnter();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-pesquisa',
    });
  }));

  it('should handle sections title color on scroll', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.onScroll();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-pesquisa',
    });
  }));

  it('should handle sections title color on resize', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.onResize();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-pesquisa',
    });
  }));
});
