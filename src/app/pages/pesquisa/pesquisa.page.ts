/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @angular-eslint/component-class-suffix */
import { VideosService } from './../../services/videos.service';
import { PesquisaService } from './../../services/pesquisa.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { handleTitleColor } from 'src/app/utils/handleTitleColor';
import { map } from 'rxjs/operators';
import { AppStore } from 'src/app/store/app.store';

@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.page.html',
  styleUrls: ['./pesquisa.page.scss'],
})
export class PesquisaPage {
  /**
   * JSON de Pesquisa,
   * Qual a pesquisa e os resultados
   */
  pesquisa: { busca: string; data: any } = {
    busca: '',
    data: [],
  };

  /**
   * Videos relacionados a busca
   */
  videos: any = [];

  /**
   * Método Construtor
   *
   * @param {ActivatedRoute} route
   * @param {PesquisaService} pesquisaService
   * @param {VideosService} videosService
   */
  constructor(
    private route: ActivatedRoute,
    private pesquisaService: PesquisaService,
    private videosService: VideosService,
    private store: AppStore,
  ) {}

  searching = false;

  /**
   * Redux Trends de busca
   * @param {Observable} trends$
   */
  trends$ = this.store
    .select((state) => state.trendsReducer)
    .pipe(map((e) => e.trends));

  /**
   * Executa toda vez em que a pagina Pesquisa será renderizada.
   */
  ionViewDidEnter(): void {
    handleTitleColor({ componentName: 'app-pesquisa' });

    this.route.queryParams.subscribe((params) => {
      if (params.busca) {
        void this.search({ value: decodeURI(params.busca) });
      }
    });
  }

  /**
   * Obtem os resultados da busca
   *
   * @param {any} busca
   */
  async search(busca: { value: string }): Promise<void> {
    try {
      this.searching = true;
      this.pesquisa.data = [];
      this.pesquisa.busca = busca.value;
      busca = { value: busca.value };
      this.pesquisa.data = await this.pesquisaService.search(
        busca.value.trim(),
      );
      this.videos = await this.videosService.getVideosFilter(
        busca.value.trim(),
      );
    } catch (error) {
      this.searching = false;
    } finally {
      this.searching = false;
    }
  }

  /**
   * Executa toda vez em que o Scroll da pagina Home é acionado.
   */
  onScroll(): void {
    handleTitleColor({ componentName: 'app-pesquisa' });
  }

  /**
   * Executa toda vez em que o Resize da pagina Home é acionado.
   */
  onResize(): void {
    handleTitleColor({ componentName: 'app-pesquisa' });
  }
}
