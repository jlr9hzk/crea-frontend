/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @angular-eslint/component-class-suffix */
import { ServicosService } from './../../services/servicos.service';
import { PesquisaService } from './../../services/pesquisa.service';
import { Component, OnInit } from '@angular/core';
import { AppStore } from '../../store/app.store';
import { map } from 'rxjs/operators';
import { VideosService } from './../../services/videos.service';
import { handleTitleColor } from 'src/app/utils/handleTitleColor';
import { handleSearchBarPosition } from 'src/app/utils/handleSearchBarPosition';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  /**
   * Método construtor
   *
   * @param {AppStore} store
   * @param {VideosService} videosService
   * @param {PesquisaService} pesquisaService
   * @param {ServicosService} servicosService
   */
  constructor(
    private store: AppStore,
    private videosService: VideosService,
    private pesquisaService: PesquisaService,
    private servicosService: ServicosService,
  ) {}

  /**
   * Guarda o estilo a ser aplicado na barra de rolagem
   */
  scrollbarStyles = `
  ::-webkit-scrollbar {
    display: none;
    width: 5px;
    height: 5px;
  }

  ::-webkit-scrollbar-track {
    background: transparent;
  }

  ::-webkit-scrollbar-thumb {
    background: transparent;
  }

  .inner-scroll {
    scrollbar-width: none;
    -ms-overflow-style: none;
  }
  `;

  /**
   * Guarda o estado do balão de sugestões.
   * Ativa e desativa atrves do input de busca
   */
  isSugestoes = false;

  placeholder: HTMLElement;
  pesquisa: HTMLElement;
  header: HTMLElement;

  /**
   * Seta o estado do balão de sugestoes
   * @param {boolean} event
   */
  setSugestoes(event: boolean): void {
    this.isSugestoes = event;
  }

  /**
   * Redux de videos
   * @param {Observable} videos$
   */
  videos$ = this.store
    .select((state) => state.videoReducer)
    .pipe(map((e) => e.videos));

  /**
   * Redux de Servicos
   * @param {Observable} servicos$
   */
  servicos$ = this.store
    .select((state) => state.servicoReducer)
    .pipe(map((e) => e.servicos));

  /**
   * Redux Trends de busca
   * @param {Observable} trends$
   */
  trends$ = this.store
    .select((state) => state.trendsReducer)
    .pipe(map((e) => e.trends));

  /**
   *  Redux Sugestões
   */
  sugestoes$ = this.store
    .select((state) => state.sugestoesReducer)
    .pipe(map((e) => e.sugestoes));

  /**
   * OnInit
   * Executa as devidas funçoes toda vez em que a página é carregada.
   */
  async ngOnInit(): Promise<void> {
    /**
     * Atualiza os Videos
     */
    await this.videosService.getVideosAllUrl('/video/getPortal');

    /**
     * Carrega Trends de Pesquisa, (Assuntos mais buscados)
     */
    await this.pesquisaService.trendsSearch();

    /**
     * Carrega os Servicos
     */
    await this.servicosService.getServicosAll();
  }

  /**
   * Executa toda vez em que a pagina Home é renderizada.
   */
  async ionViewDidEnter(): Promise<void> {
    this.placeholder = document.getElementById('placeholder');
    this.pesquisa = document.getElementById('pesquisa-sticky');
    this.header = document.getElementById('logo-bottom-space');

    handleTitleColor({ componentName: 'app-home' });

    /**
     * Carrega Sugestões de Pesquisa, (Assuntos mais buscados)
     */
    await this.pesquisaService.SuggestionSearch();
  }

  /**
   * Executa toda vez em que o Scroll da pagina Home é acionado.
   */
  onScroll(): void {
    handleSearchBarPosition({
      header: this.header,
      placeholder: this.placeholder,
      pesquisa: this.pesquisa,
    });

    handleTitleColor({ componentName: 'app-home' });
  }

  /**
   * Executa toda vez em que o Resize da pagina Home é acionado.
   */
  onResize(): void {
    handleSearchBarPosition({
      header: this.header,
      placeholder: this.placeholder,
      pesquisa: this.pesquisa,
    });

    handleTitleColor({ componentName: 'app-home' });
  }
}
