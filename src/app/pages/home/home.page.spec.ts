/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import {
  ComponentFixture,
  TestBed,
  waitForAsync,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomePage } from './home.page';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from 'ngx-webstorage/lib/core/interfaces/storageService';
import { EMPTY, Observable } from 'rxjs';
import * as handle from 'src/app/utils/handleTitleColor';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  const mockStore = {};
  const localStorageMock: StorageService = {
    store: (key: string, value: string) => {
      mockStore[key] = `${value}`;
    },
    retrieve: (key: string): string => {
      return key in mockStore ? mockStore[key] : null;
    },
    clear: (key: string) => {
      delete mockStore[key];
    },
    getStrategyName(): string {
      return 'Local';
    },
    observe(key: string): Observable<any> {
      return EMPTY;
    },
  };

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [HomePage],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        providers: [
          { provide: LocalStorageService, useValue: localStorageMock },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(HomePage);
      component = fixture.componentInstance;
      component.setSugestoes(false);
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should handle sections title color on render view', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    void component.ionViewDidEnter();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-home',
    });
  }));

  it('should handle sections title color on scroll', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.onScroll();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-home',
    });
  }));

  it('should handle sections title color on resize', fakeAsync(() => {
    const handleTitleColorSpy = jasmine.createSpy('handleTitleColorSpy');
    spyOnProperty(handle, 'handleTitleColor').and.returnValue(
      handleTitleColorSpy,
    );

    component.onResize();
    fixture.detectChanges();
    tick();

    expect(handleTitleColorSpy).toHaveBeenCalledWith({
      componentName: 'app-home',
    });
  }));
});
