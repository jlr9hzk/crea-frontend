import { Component, Renderer2 } from '@angular/core';
import { map } from 'rxjs/operators';
import { AppStore } from './store/app.store';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  browser = this.deviceService.getDeviceInfo()?.browser;
  os = this.deviceService.getDeviceInfo()?.os;

  isIE = !!(this.browser === 'IE');
  isEdge = !!(this.browser === 'MS-Edge');
  isSafari = !!(this.browser === 'Safari');
  isFirefox = !!(this.browser === 'Firefox');
  isOpera = !!(this.browser === 'Opera');
  isChrome = !!(this.browser === 'Chrome');

  isMacOs = !!(this.os === 'Mac');

  constructor(
    private store: AppStore,
    private deviceService: DeviceDetectorService,
    private renderer2: Renderer2,
  ) {}

  private shortcut1: () => void;
  private shortcut2: () => void;
  private shortcut3: () => void;
  private shortcut4: () => void;

  private helperKeyListenerUp: () => void;
  private helperKeyListenerDown: () => void;

  scrollToContent(
    target: 'conteudo' | 'menu-principal' | 'busca' | 'rodape',
  ): void {
    if (target === 'rodape') {
      const scrollContent = document.querySelectorAll('.page-content');
      scrollContent?.forEach((value) => {
        value.scrollTop = value.scrollHeight;
      });
    }
    if (target === 'conteudo') {
      const scrollContent = document.querySelectorAll('.page-content');
      scrollContent?.forEach((value) => {
        value.scrollTop = 0;
      });
    }
    document.querySelectorAll(`.${target}`).forEach((value) => {
      value.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
        inline: 'end',
      });
    });
  }

  // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
  ngOnInit(): void {
    /* ---  ATALHOS DO TECLADO ----- */

    /* -- Chrome e Edge e Safari etc... -- */
    if (
      this.isChrome ||
      this.isEdge ||
      this.isSafari ||
      this.browser.toLowerCase() === 'unknown'
    ) {
      this.shortcut1 = this.renderer2.listen(
        'document',
        'keydown.alt.1',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('conteudo');
        },
      );

      this.shortcut2 = this.renderer2.listen(
        'document',
        'keydown.alt.2',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('menu-principal');
        },
      );

      this.shortcut3 = this.renderer2.listen(
        'document',
        'keydown.alt.3',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('busca');
        },
      );

      this.shortcut4 = this.renderer2.listen(
        'document',
        'keydown.alt.4',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('rodape');
        },
      );
    }

    /* -- Firefox (not on MacOS) -- */

    if (this.isFirefox && this.isMacOs === false) {
      this.shortcut1 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            e.altKey &&
            (e.code === 'Numpad1' || e.code === 'Digit1')
          ) {
            e.preventDefault();
            this.scrollToContent('conteudo');
          }
        },
      );

      this.shortcut2 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            e.altKey &&
            (e.code === 'Numpad2' || e.code === 'Digit2')
          ) {
            e.preventDefault();
            this.scrollToContent('menu-principal');
          }
        },
      );

      this.shortcut3 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            e.altKey &&
            (e.code === 'Numpad3' || e.code === 'Digit3')
          ) {
            e.preventDefault();
            this.scrollToContent('busca');
          }
        },
      );

      this.shortcut4 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            e.altKey &&
            (e.code === 'Numpad4' || e.code === 'Digit4')
          ) {
            e.preventDefault();
            this.scrollToContent('rodape');
          }
        },
      );
    }

    /* -- Firefox on MacOS -- */

    if (this.isFirefox && this.isMacOs) {
      this.shortcut1 = this.renderer2.listen(
        'document',
        'keydown.control.alt.1',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('conteudo');
        },
      );

      this.shortcut2 = this.renderer2.listen(
        'document',
        'keydown.control.alt.2',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('menu-principal');
        },
      );

      this.shortcut3 = this.renderer2.listen(
        'document',
        'keydown.control.alt.3',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('busca');
        },
      );

      this.shortcut4 = this.renderer2.listen(
        'document',
        'keydown.control.alt.4',
        (e: KeyboardEvent) => {
          e.preventDefault();
          this.scrollToContent('rodape');
        },
      );
    }

    /* -- Opera -- */

    if (this.isOpera) {
      let esc = false;
      this.helperKeyListenerUp = this.renderer2.listen(
        'document',
        'keyup',
        (e: KeyboardEvent) => {
          if (e.code === 'Escape') {
            esc = false;
          }
        },
      );

      this.helperKeyListenerDown = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (e.shiftKey && e.code === 'Escape') {
            e.preventDefault();
            esc = true;
          }
        },
      );

      this.shortcut1 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            esc &&
            (e.code === 'Numpad1' || e.code === 'Digit1')
          ) {
            this.scrollToContent('conteudo');
          }
        },
      );

      this.shortcut2 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            esc &&
            (e.code === 'Numpad2' || e.code === 'Digit2')
          ) {
            this.scrollToContent('menu-principal');
          }
        },
      );

      this.shortcut3 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            esc &&
            (e.code === 'Numpad3' || e.code === 'Digit3')
          ) {
            this.scrollToContent('busca');
          }
        },
      );

      this.shortcut4 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.shiftKey &&
            esc &&
            (e.code === 'Numpad4' || e.code === 'Digit4')
          ) {
            this.scrollToContent('rodape');
          }
        },
      );
    }

    /* -- Internet Explorer -- */

    if (this.isIE) {
      let key = 0;
      this.helperKeyListenerUp = this.renderer2.listen(
        'document',
        'keyup',
        (e: KeyboardEvent) => {
          if (
            e.code === 'Numpad1' ||
            e.code === 'Digit1' ||
            e.code === 'Numpad2' ||
            e.code === 'Digit2' ||
            e.code === 'Numpad3' ||
            e.code === 'Digit3' ||
            e.code === 'Numpad4' ||
            e.code === 'Digit4'
          ) {
            key = 0;
          }
        },
      );

      this.helperKeyListenerDown = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (e.altKey && (e.code === 'Numpad1' || e.code === 'Digit1')) {
            e.preventDefault();
            key = 1;
          }

          if (e.altKey && (e.code === 'Numpad2' || e.code === 'Digit2')) {
            e.preventDefault();
            key = 2;
          }

          if (e.altKey && (e.code === 'Numpad3' || e.code === 'Digit3')) {
            e.preventDefault();
            key = 3;
          }

          if (e.altKey && (e.code === 'Numpad4' || e.code === 'Digit4')) {
            e.preventDefault();
            key = 4;
          }
        },
      );

      this.shortcut1 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.altKey &&
            key === 1 &&
            (e.code === 'NumpadEnter' || e.code === 'Enter')
          ) {
            e.preventDefault();
            this.scrollToContent('conteudo');
          }
        },
      );

      this.shortcut2 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.altKey &&
            key === 2 &&
            (e.code === 'NumpadEnter' || e.code === 'Enter')
          ) {
            e.preventDefault();
            this.scrollToContent('menu-principal');
          }
        },
      );

      this.shortcut3 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.altKey &&
            key === 3 &&
            (e.code === 'NumpadEnter' || e.code === 'Enter')
          ) {
            e.preventDefault();
            this.scrollToContent('busca');
          }
        },
      );

      this.shortcut4 = this.renderer2.listen(
        'document',
        'keydown',
        (e: KeyboardEvent) => {
          if (
            e.altKey &&
            key === 4 &&
            (e.code === 'NumpadEnter' || e.code === 'Enter')
          ) {
            e.preventDefault();
            this.scrollToContent('rodape');
          }
        },
      );
    }
  }

  // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
  ngOnDestroy(): void {
    this.shortcut1();
    this.shortcut2();
    this.shortcut3();
    this.shortcut4();
    this.helperKeyListenerDown();
    this.helperKeyListenerUp();
  }

  video$ = this.store
    .select((state) => state.videoReducer)
    .pipe(map((e) => e.video));
}
