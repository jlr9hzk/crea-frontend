interface SearchBarRelatedElements {
  placeholder: HTMLElement;
  pesquisa: HTMLElement;
  header: HTMLElement;
}

export function handleSearchBarPosition({
  header,
  pesquisa,
  placeholder,
}: SearchBarRelatedElements): void {
  const windowWidth = window.innerWidth;
  const bodyWidth = document.getElementById('m_width')?.clientWidth;
  const scrollWidth = windowWidth - bodyWidth;
  placeholder?.classList.remove('placeholder-active');
  pesquisa?.classList.remove('pesquisa-container');
  const offset = pesquisa?.getBoundingClientRect();
  const limit = header?.getBoundingClientRect()?.bottom;
  if (offset?.top <= limit + 10) {
    pesquisa?.classList.add('pesquisa-container');
    pesquisa?.setAttribute(
      'style',
      `top: ${limit - 1}px; width: calc(100% - ${scrollWidth}px) `,
    );
    placeholder?.classList.add('placeholder-active');
  } else {
    pesquisa?.setAttribute('style', 'width: 100%');
  }
}
