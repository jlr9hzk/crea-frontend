interface PageName {
  componentName:
    | 'app-home'
    | 'app-servicos'
    | 'app-sobre'
    | 'app-pesquisa'
    | 'app-detalhes'
    | 'app-faq'
    | 'app-acessibilidade';
}

function checkIfIsMobileLayout() {
  if (window.matchMedia('(max-width: 767px)').matches) {
    return true;
  } else {
    false;
  }
}

function getWavePosition(element: Element) {
  const tag = checkIfIsMobileLayout() ? 'wave-mobile' : 'img-wave';
  const wave = element.getElementsByClassName(tag)[0]?.getBoundingClientRect();
  return wave;
}

export function handleTitleColor({ componentName }: PageName): void {
  document.querySelectorAll('[style="z-index: 101;"]').forEach((element) => {
    if (element.tagName.toLocaleLowerCase() === componentName) {
      const { bottom } = getWavePosition(element);

      const titles = document.getElementsByClassName('title_cat');
      const lines = document.querySelectorAll('h6 + .hr');

      for (let i = 0; i < titles?.length; i += 1) {
        if (titles[i].getBoundingClientRect().bottom < bottom)
          titles[i].classList.add('title-white');

        if (titles[i].getBoundingClientRect().bottom > bottom)
          titles[i].classList.remove('title-white');
      }

      for (let i = 0; i < lines?.length; i += 1) {
        if (lines[i].getBoundingClientRect().bottom < bottom) {
          lines[i].classList.add('line-white');
        }

        if (lines[i].getBoundingClientRect().bottom > bottom) {
          lines[i].classList.remove('line-white');
        }
      }
    }
  });
}
