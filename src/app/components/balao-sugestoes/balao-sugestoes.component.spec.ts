/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-floating-promises */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BalaoSugestoesComponent } from './balao-sugestoes.component';
import { LocalStorageService } from 'ngx-webstorage';
import { Location } from '@angular/common';
import { By } from '@angular/platform-browser';
import { Router, Routes } from '@angular/router';
import { PesquisaPageModule } from 'src/app/pages/pesquisa/pesquisa.module';
describe('BalaoSugestoesComponent', () => {
  let component: BalaoSugestoesComponent;
  let fixture: ComponentFixture<BalaoSugestoesComponent>;
  let location: Location;
  let router: Router;

  const routes: Routes = [
    {
      path: 'pesquisa',
      component: PesquisaPageModule,
    },
    {
      path: 'pesquisa/:busca',
      component: PesquisaPageModule,
    },
  ];

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [BalaoSugestoesComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule.withRoutes(routes),
        ],
        providers: [LocalStorageService],
      }).compileComponents();

      fixture = TestBed.createComponent(BalaoSugestoesComponent);
      component = fixture.componentInstance;
      component.trends = [];

      router = TestBed.inject(Router);
      location = TestBed.inject(Location);

      router.initialNavigation();

      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be able to call search function', () => {
    spyOn(component, 'search');

    component.trends = [
      {
        id: 12,
        contador: 60,
        titulo: 'PREENCHIMENTO DE ART',
      },
      {
        id: 8,
        contador: 7,
        titulo: 'EMISSÃO',
      },
    ];

    fixture.detectChanges();

    const trends = fixture.debugElement.queryAll(By.css('.titulo-sugestao'));
    trends.forEach((trend) => {
      const ItemClickEvent = new Event('mousedown');
      trend.nativeElement.dispatchEvent(ItemClickEvent);
      fixture.detectChanges();
    });

    expect(component.search).toHaveBeenCalledTimes(component.trends.length);
  });

  it('should be able to search a trend', fakeAsync(() => {
    void component.search({ value: 'art' });
    fixture.detectChanges();
    tick();
    void expect(location.path()).toBe('/pesquisa?busca=art');
  }));
});
