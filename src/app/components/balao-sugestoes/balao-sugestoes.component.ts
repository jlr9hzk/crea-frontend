import { NavController } from '@ionic/angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-balao-sugestoes',
  templateUrl: './balao-sugestoes.component.html',
  styleUrls: ['./balao-sugestoes.component.scss'],
})
export class BalaoSugestoesComponent {
  /**
   * Recebe as sugestoes de pesquisa (trends)
   *
   * @param {Array} trends
   */
  @Input() trends = [];
  @Input() show = false;

  /**
   * Metodo Construtor
   *
   * @param {NavController} navCtrl
   */
  constructor(private navCtrl: NavController) {}

  /**
   * Chama pagina de pesquisa passando por parametro a sugestão de pesquisa clicada.
   *
   * @param {Array} event
   */
  async search(event: { value: string }): Promise<void> {
    await this.navCtrl.navigateForward(
      '/pesquisa?busca=' + encodeURI(event.value),
    );
  }
}
