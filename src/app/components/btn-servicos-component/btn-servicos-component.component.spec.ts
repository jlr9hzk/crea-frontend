/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BtnServicosComponentComponent } from './btn-servicos-component.component';
import { LocalStorageService } from 'ngx-webstorage';
import { By } from '@angular/platform-browser';
import { Router, Routes } from '@angular/router';
import { Location } from '@angular/common';
import { PesquisaPageModule } from 'src/app/pages/pesquisa/pesquisa.module';
describe('BtnServicosComponentComponent', () => {
  let component: BtnServicosComponentComponent;
  let fixture: ComponentFixture<BtnServicosComponentComponent>;
  let location: Location;
  let router: Router;

  const routes: Routes = [
    {
      path: 'pesquisa',
      component: PesquisaPageModule,
    },
    {
      path: 'pesquisa/:busca',
      component: PesquisaPageModule,
    },
  ];

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [BtnServicosComponentComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule.withRoutes(routes),
        ],
        providers: [LocalStorageService],
      }).compileComponents();

      fixture = TestBed.createComponent(BtnServicosComponentComponent);
      component = fixture.componentInstance;
      component.servico = '';

      router = TestBed.inject(Router);
      location = TestBed.inject(Location);

      router.initialNavigation();

      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should able to call search function on click', () => {
    spyOn(component, 'search');
    const service = fixture.debugElement.query(By.css('#card'))?.nativeElement;

    service?.click();

    expect(component.search).toHaveBeenCalled();
  });

  it('should be able to search a service', fakeAsync(() => {
    component.search('certidao');
    fixture.detectChanges();
    tick();
    expect(location.path()).toBe('/pesquisa?busca=certidao');
  }));
});
