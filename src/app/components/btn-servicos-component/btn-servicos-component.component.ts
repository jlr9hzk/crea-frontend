import { URL_SERVE } from '../../../environments/environment';
import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-btn-servicos-component',
  templateUrl: './btn-servicos-component.component.html',
  styleUrls: ['./btn-servicos-component.component.scss'],
})
export class BtnServicosComponentComponent {
  @Input() servico;

  url_serve = URL_SERVE;
  /**
   * Metodo Construtor
   *
   * @param {NavController} navCtrl
   */
  constructor(private navCtrl: NavController) {}

  /**
   * Chama pagina de pesquisa passando por parametro o servico clicado.
   *
   * @param {string} query
   */
  search(query: string): void {
    if (query?.toLowerCase() === 'cursos e eventos') {
      window.open('https://www.creasp.org.br/curso/', '_blank');
    } else {
      void this.navCtrl.navigateForward(`/pesquisa?busca=${encodeURI(query)}`);
    }
  }
}
