import { ConteudoDetalhesComponent } from './conteudo-detalhes/conteudo-detalhes.component';
import { ResultadoComponentComponent } from './resultado-component/resultado-component.component';
import { ChatbotComponentComponent } from './chatbot-component/chatbot-component.component';
import { BalaoSugestoesComponent } from './balao-sugestoes/balao-sugestoes.component';
import { BtnServicosComponentComponent } from './btn-servicos-component/btn-servicos-component.component';
import { VideosComponentComponent } from './videos-component/videos-component.component';
import { PesquisaComponentComponent } from './pesquisa-component/pesquisa-component.component';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoModalComponent } from './video-modal/video-modal.component';
import { Header2Component } from './header2/header2.component';
import { SafeHtmlPipeModule } from '../safe-html.module';

@NgModule({
  declarations: [
    HeaderComponent,
    Header2Component,
    FooterComponent,
    PesquisaComponentComponent,
    VideosComponentComponent,
    BtnServicosComponentComponent,
    BalaoSugestoesComponent,
    ChatbotComponentComponent,
    ResultadoComponentComponent,
    VideoModalComponent,
    ConteudoDetalhesComponent,
  ],
  imports: [IonicModule, CommonModule, RouterModule, SafeHtmlPipeModule],
  exports: [
    HeaderComponent,
    Header2Component,
    FooterComponent,
    PesquisaComponentComponent,
    VideosComponentComponent,
    BtnServicosComponentComponent,
    BalaoSugestoesComponent,
    ChatbotComponentComponent,
    ResultadoComponentComponent,
    VideoModalComponent,
    ConteudoDetalhesComponent,
  ],
})
export class ComponentsModule {}
