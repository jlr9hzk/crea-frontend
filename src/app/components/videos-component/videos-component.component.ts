/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Component, Input, OnInit } from '@angular/core';

import { AppStore } from 'src/app/store/app.store';
import { openVideoPlayer } from 'src/app/store/modules/videos/videos.action';

interface IVideo {
  id: number;
  titulo: string;
  urlVideo: string;
  descricao: string;
}

@Component({
  selector: 'app-videos-component',
  templateUrl: './videos-component.component.html',
  styleUrls: ['./videos-component.component.scss'],
})
export class VideosComponentComponent implements OnInit {
  /**
   * Recebe os videos a serem exibidos na lista.
   *
   * @param {Array} videos
   */
  @Input() videos;

  @Input() hidetitle;

  @Input() titleSize;

  @Input() titleColor: string;

  /**
   * Guarda o video a ser reproduzido
   *
   * @param {IVideo} showVideoModal
   */
  showVideoModal: IVideo;

  /**
   * Metodo Construtor
   * @param {AppStore} store
   */
  constructor(private store: AppStore) {}

  /**
   * OnInit
   *
   * Executa toda ver que a view é carregada
   */
  ngOnInit(): void {
    this.showVideoModal = this.videos[0];
  }

  /**
   * Abre o modal de video, passando o video a ser reproduzido
   *
   * @param {IVideo} video
   */
  showModal(video: IVideo): void {
    document
      .getElementsByTagName('app-chatbot-component')[0]
      ?.setAttribute('style', 'display: none');
    this.store.dispatch(openVideoPlayer({ video: video }));
  }
}
