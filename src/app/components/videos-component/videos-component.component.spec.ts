/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { VideosComponentComponent } from './videos-component.component';
import { LocalStorageService } from 'ngx-webstorage';
import { By } from '@angular/platform-browser';
import { AppStore } from 'src/app/store/app.store';
import { openVideoPlayer } from 'src/app/store/modules/videos/videos.action';
describe('VideosComponentComponent', () => {
  let component: VideosComponentComponent;
  let fixture: ComponentFixture<VideosComponentComponent>;
  let store: AppStore;
  const fakeVideos = [
    {
      id: 10,
      titulo: 'Transformação Digital do Crea-SP',
      descricao:
        'O CREA-SP começou a maior transformação de todos os tempos. Estamos construindo uma nova era de inovações para nos tornar uma plataforma de serviços que seja referência, com o profissional no centro da nossa transformação digital.',
      urlVideo: 'https://www.youtube.com/embed/FadiJJdASyM',
    },
  ];

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [VideosComponentComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        providers: [LocalStorageService],
      }).compileComponents();

      fixture = TestBed.createComponent(VideosComponentComponent);
      store = TestBed.inject(AppStore);
      component = fixture.componentInstance;
      component.videos = [];
      component.showVideoModal;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should able to show video player on click in thumbnails', () => {
    spyOn(component, 'showModal');
    component.videos = fakeVideos;
    fixture.detectChanges();
    const thumbnails = fixture.debugElement.queryAll(By.css('.cover'));
    thumbnails.forEach((thumbnail) => {
      const ItemClickEvent = new Event('click');
      thumbnail.nativeElement.dispatchEvent(ItemClickEvent);
      fixture.detectChanges();
    });

    expect(component.showModal).toHaveBeenCalledTimes(component.videos.length);
  });

  it('should storage video in global state and show video modal', fakeAsync(() => {
    spyOn(store, 'dispatch');
    component.showModal(fakeVideos[0]);
    fixture.detectChanges();
    tick();
    expect(store.dispatch).toHaveBeenCalledWith(
      openVideoPlayer({ video: fakeVideos[0] }),
    );
  }));
});
