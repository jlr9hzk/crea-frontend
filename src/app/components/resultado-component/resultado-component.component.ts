import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-resultado-component',
  templateUrl: './resultado-component.component.html',
  styleUrls: ['./resultado-component.component.scss'],
})
export class ResultadoComponentComponent {
  /**
   * Recebo o detalhes do resultado da pesquisa a ser exibido na tela.
   */
  @Input() resultado;

  /**
   * Metodo Construtor
   *
   * @param {NavController} navCtrl
   */
  constructor(private navCtrl: NavController) {}

  /**
   * Vai para a tela de detalhes do item de pesquisa selecionado, passando o ID da pesquisa selecionada.
   *
   * @param {number} id
   */
  goToDetalhes(id: number): void {
    void this.navCtrl.navigateForward(`/detalhes?id=${id}`);
  }
}
