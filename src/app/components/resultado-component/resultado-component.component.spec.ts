import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResultadoComponentComponent } from './resultado-component.component';
import { LocalStorageService } from 'ngx-webstorage';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, Routes } from '@angular/router';
import { Location } from '@angular/common';
import { DetalhesPageModule } from 'src/app/pages/detalhes/detalhes.module';
import { SafeHtmlPipe } from 'src/app/safe-html.pipe';

describe('ResultadoComponentComponent', () => {
  let component: ResultadoComponentComponent;
  let fixture: ComponentFixture<ResultadoComponentComponent>;
  let router: Router;
  let location: Location;

  const routes: Routes = [
    {
      path: 'detalhes',
      component: DetalhesPageModule,
    },
  ];

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [ResultadoComponentComponent, SafeHtmlPipe],
        imports: [
          IonicModule.forRoot(),
          RouterTestingModule.withRoutes(routes),
        ],
        providers: [LocalStorageService],
      }).compileComponents();

      fixture = TestBed.createComponent(ResultadoComponentComponent);
      component = fixture.componentInstance;
      component.resultado = [];

      router = TestBed.inject(Router);
      location = TestBed.inject(Location);

      router.initialNavigation();

      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should navigate no details page', fakeAsync(() => {
    component.goToDetalhes(10);
    fixture.detectChanges();
    tick();
    void expect(location.path()).toBe('/detalhes?id=10');
  }));
});
