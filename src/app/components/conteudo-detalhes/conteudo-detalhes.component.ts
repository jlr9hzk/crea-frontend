import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-conteudo-detalhes',
  // templateUrl: './conteudo-detalhes.component.html',
  template: '<p [innerHTML]="conteudo | safeHtml"></p>',
  styleUrls: ['./conteudo-detalhes.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ConteudoDetalhesComponent {
  /**
   * Recebe os detalhes da pesquisa selecionada
   *
   * @param {string} conteudo
   */
  @Input() conteudo = {};

  /**
   * @ignore
   */
  constructor() {
    //
  }

  /**
   * Limpa a a variavel ao fechar o componente
   */
  // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
  ngOnDestroy(): void {
    this.conteudo = undefined;
  }
}
