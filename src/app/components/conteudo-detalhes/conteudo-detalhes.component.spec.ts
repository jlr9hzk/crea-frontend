import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { SafeHtmlPipe } from 'src/app/safe-html.pipe';

import { ConteudoDetalhesComponent } from './conteudo-detalhes.component';

describe('ConteudoDetalhesComponent', () => {
  let component: ConteudoDetalhesComponent;
  let fixture: ComponentFixture<ConteudoDetalhesComponent>;

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [ConteudoDetalhesComponent, SafeHtmlPipe],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(ConteudoDetalhesComponent);
      component = fixture.componentInstance;
      component.conteudo;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });
});
