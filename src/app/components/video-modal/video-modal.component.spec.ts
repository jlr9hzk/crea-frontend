/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { AppStore } from 'src/app/store/app.store';
import { closeVideoPlayer } from 'src/app/store/modules/videos/videos.action';

import { IVideo, VideoModalComponent } from './video-modal.component';

describe('VideoModalComponent', () => {
  let component: VideoModalComponent;
  let fixture: ComponentFixture<VideoModalComponent>;
  let store: AppStore;

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [VideoModalComponent],
        imports: [IonicModule.forRoot()],
      }).compileComponents();

      fixture = TestBed.createComponent(VideoModalComponent);
      component = fixture.componentInstance;
      component.video = {} as IVideo;
      store = TestBed.inject(AppStore);
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should be able to call close video player function on click in close button', () => {
    spyOn(component, 'closeModal');
    const closeButton = fixture.debugElement.query(By.css('button'));
    closeButton.nativeElement.click();
    fixture.detectChanges();
    expect(component.closeModal).toHaveBeenCalled();
  });

  it('should remove video from global state and close video modal', fakeAsync(() => {
    spyOn(store, 'dispatch');
    component.closeModal();
    fixture.detectChanges();
    tick();
    expect(store.dispatch).toHaveBeenCalledWith(closeVideoPlayer());
  }));
});
