import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { AppStore } from 'src/app/store/app.store';
import { closeVideoPlayer } from 'src/app/store/modules/videos/videos.action';

export interface IVideo {
  id: number;
  titulo: string;
  urlVideo: string;
  descricao: string;
  safeUrl?: SafeResourceUrl;
}

@Component({
  selector: 'app-video-modal',
  templateUrl: './video-modal.component.html',
  styleUrls: ['./video-modal.component.scss'],
})
export class VideoModalComponent implements OnInit {
  /**
   * Recebe o video a ser reproduzido no canto inferior das paginas
   *
   * @param {IVideo} video
   */
  @Input() video: IVideo;

  /**
   * Estado do modal (reprodução do video), se ativo ou fechar.
   *
   * @param {boolean} show
   */
  @Input() show: boolean;

  /**
   * Metodo construtor
   *
   * @param {DomSanitizer} sanitizer
   * @param {AppStore} store
   */
  constructor(private sanitizer: DomSanitizer, private store: AppStore) {}

  /**
   * OnInit
   *
   * Emite toda vez que é carregado o view
   * Carrega o video recebido
   */
  ngOnInit(): void {
    this.video = {
      ...this.video,
      safeUrl: this.sanitizer.bypassSecurityTrustResourceUrl(
        this.video.urlVideo + '?autoplay=1',
      ),
    };
  }

  /**
   * Limpa a a variavel ao fechar o componente
   * Para a reprodução do video no canto da tela, fechando o modal do video.
   */
  // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
  ngOnDestroy(): void {
    this.video = undefined;
  }

  /**
   * Para a reprodução e fecha o modal.
   * Mostra novamente o ChatBot novamente.
   */
  closeModal(): void {
    document
      .getElementsByTagName('app-chatbot-component')[0]
      ?.removeAttribute('style');
    this.store.dispatch(closeVideoPlayer());
  }
}
