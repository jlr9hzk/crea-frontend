/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { LocalStorageService } from 'ngx-webstorage';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.scss'],
})
export class Header2Component implements OnInit {
  /**
   * Indica o estado do contraste das paginas.
   *
   * @param {boolean} isContrast
   */
  isContrast: boolean;

  @Input() showBgHome = false;
  @Input() showLogoBottomSpace = false;
  @Input() wavePosition: number;

  /**
   * Metodo Construtor
   *
   * @param {LocalStorageService} localSt
   */
  constructor(private localSt: LocalStorageService) {}

  /**
   * OnInit
   *
   * Inicias e verifica as respectivas funções ao inializar o componente
   */
  async ngOnInit(): Promise<void> {
    this.isContrast = (await this.localSt.retrieve('contrast')) || false;
    if (this.isContrast) {
      this.isContrast = false;
      this.localSt.store('contrast', false);
    } else {
      this.isContrast = true;
      this.localSt.store('contrast', true);
    }
    this.contraste();
  }

  /**
   * Função de contraste, altera as cores da pagina
   */
  contraste(): void {
    if (!this.isContrast) {
      const teste = document.querySelectorAll('.contraste-branco');
      teste.forEach((element) => {
        element.classList.remove('contraste-branco');
        element.classList.add('contraste-preto');
      });

      const contrastContent = document.querySelectorAll('.ion-content');
      contrastContent.forEach((element) => {
        element.classList.remove('ion-content');
        element.classList.add('contrast-ion-content');
      });

      const contrastFont = document.querySelectorAll('.contraste-font-white');
      contrastFont.forEach((element) => {
        element.classList.remove('contraste-font-white');
        element.classList.add('contraste-font-amarelo');
      });

      const contrastChat = document.querySelectorAll('.chat-boot-no-contrast');
      contrastChat.forEach((element) => {
        element.classList.remove('chat-boot-no-contrast');
        element.classList.add('chat-boot-contrast');
      });

      this.localSt.store('contrast', true);
      this.isContrast = true;
    } else {
      const teste = document.querySelectorAll('.contraste-preto');
      teste.forEach((element) => {
        element.classList.remove('contraste-preto');
        element.classList.add('contraste-branco');
      });

      const contrastContent = document.querySelectorAll(
        '.contrast-ion-content',
      );
      contrastContent.forEach((element) => {
        element.classList.remove('contrast-ion-content');
        element.classList.add('ion-content');
      });

      const contrastFont = document.querySelectorAll('.contraste-font-amarelo');
      contrastFont.forEach((element) => {
        element.classList.remove('contraste-font-amarelo');
        element.classList.add('contraste-font-white');
      });

      const contrastChat = document.querySelectorAll('.chat-boot-contrast');
      contrastChat.forEach((element) => {
        element.classList.remove('chat-boot-contrast');
        element.classList.add('chat-boot-no-contrast');
      });

      this.localSt.store('contrast', false);
      this.isContrast = false;
    }
  }

  /**
   * Abre o menu Mobile
   */
  openNav(): void {
    const element = document.querySelectorAll('.navclose');
    const navshadow = document.querySelectorAll('.navshadow-none');
    for (let i = 0; i < element.length; i++) {
      element[i].classList.remove('navclose');
      element[i].classList.add('navopen');
    }
    for (let i = 0; i < navshadow.length; i++) {
      navshadow[i].classList.remove('navshadow-none');
      navshadow[i].classList.add('navshadow');
    }
  }

  /**
   * Fecha o menu Mobile
   */
  closeNav(): void {
    const element = document.querySelectorAll('.navopen');
    const navshadow = document.querySelectorAll('.navshadow');
    for (let i = 0; i < element.length; i++) {
      element[i].classList.remove('navopen');
      element[i].classList.add('navclose');
    }

    for (let i = 0; i < navshadow.length; i++) {
      navshadow[i].classList.remove('navshadow');
      navshadow[i].classList.add('navshadow-none');
    }
  }

  /**
   * Acessibilidade, vai para o conteudo selecionado quando clicado no link de acessibilidade.
   *
   * @param {string} target
   */
  scrollTo(target: string): void {
    if (target === 'rodape') {
      const scrollContent = document.querySelectorAll('.page-content');
      scrollContent?.forEach((value) => {
        value.scrollTop = value.scrollHeight;
      });
    }
    if (target === 'conteudo') {
      const scrollContent = document.querySelectorAll('.page-content');
      scrollContent?.forEach((value) => {
        value.scrollTop = 0;
      });
    }
    document.querySelectorAll(`.${target}`).forEach((value) => {
      value.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
        inline: 'end',
      });
    });
  }
}
