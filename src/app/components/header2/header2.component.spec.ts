/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-floating-promises */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Header2Component } from './header2.component';
import { LocalStorageService } from 'ngx-webstorage';
import { StorageService } from 'ngx-webstorage/lib/core/interfaces/storageService';
import { EMPTY, Observable } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Router, Routes } from '@angular/router';
import { Location } from '@angular/common';
import { SobrePageModule } from 'src/app/pages/sobre/sobre.module';
import { HomePageModule } from 'src/app/pages/home/home.module';
import { ServicosPageModule } from 'src/app/pages/servicos/servicos.module';
import { FaqPageModule } from 'src/app/pages/faq/faq.module';
import { PesquisaPageModule } from '../../pages/pesquisa/pesquisa.module';
import { DetalhesPageModule } from 'src/app/pages/detalhes/detalhes.module';
describe('Header2Component', () => {
  let component: Header2Component;
  let fixture: ComponentFixture<Header2Component>;
  let router: Router;
  let location: Location;
  const routes: Routes = [
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full',
    },
    {
      path: 'sobre',
      component: SobrePageModule,
    },
    {
      path: 'home',
      component: HomePageModule,
    },
    {
      path: 'servicos',
      component: ServicosPageModule,
    },
    {
      path: 'faq',
      component: FaqPageModule,
    },
    {
      path: 'pesquisa',
      component: PesquisaPageModule,
    },
    {
      path: 'pesquisa/:busca',
      component: PesquisaPageModule,
    },
    {
      path: 'detalhes',
      component: DetalhesPageModule,
    },
  ];

  const mockStore = {};
  mockStore['contrast'] = true;
  const localStorageMock: StorageService = {
    store: (key: string, value: string) => {
      mockStore[key] = `${value}`;
    },
    retrieve: (key: string): string => {
      return key in mockStore ? mockStore[key] : null;
    },
    clear: (key: string) => {
      delete mockStore[key];
    },
    getStrategyName(): string {
      return 'Local';
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    observe(key: string): Observable<any> {
      return EMPTY;
    },
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [Header2Component],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule.withRoutes(routes),
        ],
        providers: [
          { provide: LocalStorageService, useValue: localStorageMock },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(Header2Component);
      component = fixture.componentInstance;
      // component.isContrast = true;
      // component.contraste();
      // component.isContrast = false;
      component.openNav();
      component.closeNav();
      component.scrollTo('menu-principal');
      fixture.detectChanges();

      router = TestBed.inject(Router);
      location = TestBed.inject(Location);

      router.initialNavigation();
    }),
  );

  it('should create', () => {
    const element1 = document.createElement('div');
    element1.setAttribute('class', 'ion-content');
    const element2 = document.createElement('div');
    element2.setAttribute('class', 'contraste-font-white');
    const element3 = document.createElement('div');
    element3.setAttribute('class', 'chat-boot-no-contrast');
    const element4 = document.createElement('div');
    element4.setAttribute('class', 'contraste-preto');
    const element5 = document.createElement('div');
    element5.setAttribute('class', 'contrast-ion-content');
    const element6 = document.createElement('div');
    element6.setAttribute('class', 'contraste-font-amarelo');
    const element7 = document.createElement('div');
    element7.setAttribute('class', 'chat-boot-contrast');

    expect(element1).toBeTruthy();
    expect(element2).toBeTruthy();
    expect(element3).toBeTruthy();
    expect(element4).toBeTruthy();
    expect(element5).toBeTruthy();
    expect(element6).toBeTruthy();
    expect(element7).toBeTruthy();

    expect(component).toBeTruthy();
  });

  it('should be able to toggle contrast', fakeAsync(() => {
    expect(component.isContrast).toBeTrue();
    const checkbox =
      fixture.debugElement.nativeElement.querySelector('#alto-contraste');
    checkbox.click();
    fixture.detectChanges();
    tick();
    expect(component.isContrast).toBeFalse();

    checkbox.click();
    fixture.detectChanges();
    tick();
    expect(component.isContrast).toBeTrue();
  }));

  it('should be able to scroll to content', fakeAsync(() => {
    spyOn(component, 'scrollTo');
    const linkConteudo = fixture.debugElement.queryAll(By.css('.anchor'))[0]
      .nativeElement;
    linkConteudo.click();
    fixture.detectChanges();
    tick();
    expect(component.scrollTo).toHaveBeenCalledWith('conteudo');
  }));

  it('should be able to scroll to menu', fakeAsync(() => {
    spyOn(component, 'scrollTo');
    const linkMenu = fixture.debugElement.queryAll(By.css('.anchor'))[1]
      .nativeElement;
    linkMenu.click();
    fixture.detectChanges();
    tick();
    expect(component.scrollTo).toHaveBeenCalledWith('menu-principal');
  }));

  it('should be able to scroll to search', fakeAsync(() => {
    spyOn(component, 'scrollTo');
    const linkSearch = fixture.debugElement.queryAll(By.css('.anchor'))[2]
      .nativeElement;
    linkSearch.click();
    fixture.detectChanges();
    tick();
    expect(component.scrollTo).toHaveBeenCalledWith('busca');
  }));

  it('should be able to scroll to footer', fakeAsync(() => {
    spyOn(component, 'scrollTo');
    const linkFooter = fixture.debugElement.queryAll(By.css('.anchor'))[3]
      .nativeElement;
    linkFooter.click();
    fixture.detectChanges();
    tick();
    expect(component.scrollTo).toHaveBeenCalledWith('rodape');
  }));

  it('should able to navigate on click in a menu option', fakeAsync(() => {
    const options = fixture.debugElement.queryAll(By.css('.nav-link'));
    options.forEach((option) => {
      option.nativeElement.click();
      fixture.detectChanges();
      tick();
      expect(location.path()).toBe(option.nativeElement.getAttribute('href'));
    });
  }));

  it('should be able to navigate to home on click in the logo', fakeAsync(() => {
    const logo = fixture.debugElement
      .query(By.css('.logo'))
      ?.query(By.css('a'))?.nativeElement;
    logo.click();
    fixture.detectChanges();
    tick();
    expect(location.path()).toBe(logo.getAttribute('href'));
  }));
});
