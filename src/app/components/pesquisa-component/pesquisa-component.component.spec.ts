/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule, NavController } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PesquisaComponentComponent } from './pesquisa-component.component';
import { LocalStorageService } from 'ngx-webstorage';
import { Location } from '@angular/common';
import { Router, Routes } from '@angular/router';
import { PesquisaPageModule } from 'src/app/pages/pesquisa/pesquisa.module';

describe('PesquisaComponentComponent', () => {
  let component: PesquisaComponentComponent;
  let fixture: ComponentFixture<PesquisaComponentComponent>;
  let router: Router;
  let location: Location;
  const routes: Routes = [
    {
      path: 'pesquisa',
      component: PesquisaPageModule,
    },
    {
      path: 'pesquisa/:busca',
      component: PesquisaPageModule,
    },
  ];

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [PesquisaComponentComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule.withRoutes(routes),
        ],
        providers: [LocalStorageService, { provide: NavController }],
      }).compileComponents();

      fixture = TestBed.createComponent(PesquisaComponentComponent);
      router = TestBed.inject(Router);
      location = TestBed.inject(Location);

      component = fixture.componentInstance;
      component.isSugestoes;
      component.trend;
      component.setSugestoes();
      component.dismissSugestoes();
      component.onKey(new KeyboardEvent('keydown'));
      fixture.detectChanges();

      router.initialNavigation();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should be able to call autocomplete function on input type', fakeAsync(() => {
    spyOn(component, 'autoComplete');
    const searchInput: HTMLInputElement =
      fixture.nativeElement.querySelector('input');
    tick();
    searchInput.value = 'EM';
    const InputEvent = new CustomEvent('input', {
      detail: {
        inputType: 'insertText',
      },
    });
    searchInput?.dispatchEvent(InputEvent);
    fixture.detectChanges();
    tick();
    expect(component.autoComplete).toHaveBeenCalled();
  }));

  it('should be able to autocomplete uppercase text', () => {
    component.trends = [{ contador: 4, id: 1, assunto: 'EMISSÃO' }];
    component.autoComplete({
      target: { value: 'EM' },
      detail: { inputType: 'insertText' },
    });
    expect(component.trend).toEqual('EMISSÃO');
  });

  it('should be able to autocomplete lowercase text', () => {
    component.trends = [{ contador: 4, id: 1, assunto: 'EMISSÃO' }];
    component.autoComplete({
      target: { value: 'em' },
      detail: { inputType: 'insertText' },
    });
    expect(component.trend).toEqual('emissão');
  });

  it('should be able to autocomplete capitalized text', () => {
    component.trends = [{ contador: 4, id: 1, assunto: 'EMISSÃO' }];
    component.autoComplete({
      target: { value: 'Em' },
      detail: { inputType: 'insertText' },
    });
    expect(component.trend).toEqual('Emissão');
  });

  it('should define trend as empty string if input value is empty string', () => {
    component.trends = [{ contador: 4, id: 1, assunto: 'EMISSÃO' }];
    component.autoComplete({
      target: { value: '' },
      detail: { inputType: 'insertText' },
    });
    expect(component.trend).toEqual('');
  });

  it("should undefine trend if input value don't match any trend of trends array", () => {
    component.trends = [{ contador: 4, id: 1, assunto: 'EMISSÃO' }];
    component.autoComplete({
      target: { value: 'nadacomnada' },
      detail: { inputType: 'insertText' },
    });
    expect(component.trend).toBeUndefined();
  });

  it('should be able to call search function on press Enter key', fakeAsync(() => {
    spyOn(component, 'search');
    const searchInput: HTMLInputElement =
      fixture.nativeElement.querySelector('input');
    tick();

    const KeyupEvent: any = new KeyboardEvent('keyup', {
      key: 'enter',
      bubbles: true,
    });

    searchInput?.dispatchEvent(KeyupEvent);
    fixture.detectChanges();
    tick();

    expect(component.search).toHaveBeenCalled();
  }));

  it('should be able to call onKey function on press Tab key', fakeAsync(() => {
    spyOn(component, 'onKey');
    const searchInput: HTMLInputElement =
      fixture.nativeElement.querySelector('input');
    tick();

    const KeydownEvent: any = new KeyboardEvent('keydown', {
      key: 'tab',
      bubbles: true,
    });

    searchInput?.dispatchEvent(KeydownEvent);
    fixture.detectChanges();
    tick();

    expect(component.onKey).toHaveBeenCalled();
  }));

  it('should autocomplete on call key event', () => {
    component.trend = 'somevalue';
    const KeydownEvent: any = new KeyboardEvent('keydown', {
      key: 'tab',
      code: 'Tab',
      bubbles: true,
    });
    component.onKey(KeydownEvent);
    fixture.detectChanges();
    expect(component.trend).toBe('');
  });

  it('should search by art', fakeAsync(() => {
    component.search({ value: 'art' });
    fixture.detectChanges();
    tick();
    expect(location.path()).toBe('/pesquisa?busca=art');
  }));

  it('should able to click in search image', fakeAsync(() => {
    const searchInput: HTMLInputElement =
      fixture.nativeElement.querySelector('input');
    tick();
    searchInput.value = 'ART';
    fixture.detectChanges();

    spyOn(component, 'search_img');

    const searchImage: HTMLImageElement =
      fixture.nativeElement.querySelector('.icon-search');
    tick();

    const imageClickEvent = new Event('click');

    searchImage?.dispatchEvent(imageClickEvent);
    expect(imageClickEvent.target).toEqual(searchImage);

    searchImage.click();
    fixture.detectChanges();

    expect(component.search_img).toHaveBeenCalled();
  }));

  it('should search on call search_img function', fakeAsync(() => {
    component.search_img();
    fixture.detectChanges();
    tick();

    expect(location.path()).toContain('/pesquisa?busca');
  }));
});
