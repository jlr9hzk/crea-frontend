/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { NavController } from '@ionic/angular';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy,
} from '@angular/core';

@Component({
  selector: 'app-pesquisa-component',
  templateUrl: './pesquisa-component.component.html',
  styleUrls: ['./pesquisa-component.component.scss'],
})
export class PesquisaComponentComponent implements OnInit, OnDestroy {
  /**
   * Emite o estado para o balão de sugestoes da Home.
   *
   * @param {boolean} isSugestoes
   */
  @Output() isSugestoes = new EventEmitter<boolean>();

  /**
   * Recebe as trends (sugestoes) de pesquisa, sugestoes dos itens mais buscados
   *
   * @param {any} trends
   */
  @Input() trends;
  @Input() sugestoes;

  /**
   * Trend de pesquisa, palavra selecionada
   */
  trend: string;

  /**
   * Metodo construtor
   *
   * @param {NavController} navCtrl
   */
  constructor(private navCtrl: NavController) {}

  /**
   * OnInit
   *
   * Emite o estado para o balão de sugestoes
   */
  ngOnInit(): void {
    this.trend = '';
    this.isSugestoes.emit(false);
  }

  /**
   * Ativa o balão de sugestoes da home
   */
  setSugestoes(): void {
    this.isSugestoes.emit(true);
  }

  /**
   * Desativa o balão de sugestoes
   */
  dismissSugestoes(): void {
    this.isSugestoes.emit(false);
  }

  /**
   * Direciona para a pagina de resultados, passando o parametro de busca.
   *
   * @param {Array} event
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  search(event: any): void {
    this.isSugestoes.emit(false);
    void this.navCtrl.navigateForward(
      '/pesquisa?busca=' + encodeURI(event.value),
    );
    this.trend = '';
    (document.getElementById('search') as HTMLInputElement).value = '';
  }

  /**
   * Quando clica na img da lupa, aciona essa função. Onde pega o valor do input e aciona a pesquisa
   */
  search_img(): void {
    let valor = '';
    document.querySelectorAll('#search').forEach((input) => {
      if ((input as HTMLInputElement).value.length > 0) {
        valor = (input as HTMLInputElement).value;
      }
    });
    // const valor = (document.querySelectorAll('search') as HTMLInputElement).value;
    void this.navCtrl.navigateForward('/pesquisa?busca=' + encodeURI(valor));
    this.trend = '';
  }
  /**
   * Deixa a primeira letra do texto maiúscula.
   * @param {string} string
   * @returns {string} string
   */

  capitalizeFirstLetter(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }

  removerSpecials(text: string): string {
    // eliminando acentuação
    text = text?.replace(/[ÀÁÂÃÄÅ]/, 'A');
    text = text?.replace(/[àáâãäå]/, 'a');
    text = text?.replace(/[ÈÉÊË]/, 'E');
    text = text?.replace(/[èéêë]/, 'e');
    text = text?.replace(/[ÌÍÎĨ]/, 'I');
    text = text?.replace(/[ìíîĩ]/, 'i');
    text = text?.replace(/[ÒÓÔÕ]/, 'O');
    text = text?.replace(/[òóôõ]/, 'o');
    text = text?.replace(/[ÙÚÛŨ]/, 'U');
    text = text?.replace(/[ùúûũ]/, 'u');
    text = text.replace(/[Ç]/, 'C');
    text = text?.replace(/[ç]/, 'c');
    return text;
  }

  /**
   * Obtem as sugestoes de palavras de busca de acordo com a digitação.
   *
   * @param {any} event
   */

  autoComplete(event: { target?: any; detail?: any }): void {
    /* AutoCompletar  */
    if (event.detail?.inputType?.toLowerCase() === 'inserttext') {
      const value = event.target.value;
      if (value === '') {
        this.trend = '';
      } else {
        this.trend = '';
        if (value === value?.toLowerCase()) {
          let completes = this.trends?.filter((trend: { assunto: string }) =>
            trend.assunto?.toLowerCase().startsWith(value?.toLowerCase()),
          );

          if (completes?.length === 0) {
            completes = this.trends?.filter((trend: { assunto: string }) =>
              this.removerSpecials(trend.assunto)
                ?.toLowerCase()
                .startsWith(value?.toLowerCase()),
            );
          }

          let suggestions = this.sugestoes?.filter(
            (suggestion: { titulo: string }) =>
              suggestion.titulo?.toLowerCase().includes(value?.toLowerCase()),
          );

          if (suggestions?.length === 0) {
            suggestions = this.sugestoes?.filter(
              (suggestion: { titulo: string }) =>
                this.removerSpecials(suggestion.titulo)
                  ?.toLocaleLowerCase()
                  ?.includes(value?.toLowerCase()),
            );
          }

          this.trend = completes[0]?.assunto?.toLowerCase();

          if (suggestions?.length > 0) {
            if (document.getElementById('match-sugestion')) {
              document.getElementById('match-sugestion').innerHTML = '';
              suggestions.forEach((value, index) => {
                if (index < 5) {
                  const element = document.createElement('p');
                  element.style.fontSize = '13px';
                  element.style.marginBottom = '1px';
                  element.style.textDecoration = 'underline';
                  element.style.cursor = 'pointer';
                  element.addEventListener('click', () => {
                    this.search({ value: value.titulo });
                  });
                  element.innerText = value.titulo;
                  document
                    .getElementById('match-sugestion')
                    .insertAdjacentElement('beforeend', element);
                  // if (index < completes.length - 1)
                  //   document
                  //     .getElementById('match-sugestion')
                  //     .insertAdjacentText('beforeend', ', ');
                }
              });
            }
          } else {
            if (document.getElementById('match-sugestion'))
              document.getElementById('match-sugestion').innerHTML = '';
          }
        } else if (value === this.capitalizeFirstLetter(value)) {
          this.trends?.forEach((trend: { assunto: any }) => {
            if (
              this.capitalizeFirstLetter(trend.assunto)?.startsWith(
                this.capitalizeFirstLetter(value),
              ) ||
              this.removerSpecials(
                this.capitalizeFirstLetter(trend.assunto),
              )?.startsWith(this.capitalizeFirstLetter(value))
            ) {
              this.trend = this.capitalizeFirstLetter(trend.assunto);
            }
          });

          let contSuggestion = 0;
          if (document.getElementById('match-sugestion'))
            document.getElementById('match-sugestion').innerHTML = '';
          this.sugestoes?.forEach((suggestion: { titulo: any }) => {
            if (
              suggestion.titulo.toLowerCase().includes(value.toLowerCase()) ||
              this.removerSpecials(suggestion.titulo)
                ?.toLowerCase()
                .includes(value.toLowerCase())
            ) {
              if (contSuggestion < 5) {
                if (document.getElementById('match-sugestion')) {
                  // document.getElementById('match-sugestion').innerHTML = '';
                  const element = document.createElement('p');
                  element.style.fontSize = '13px';
                  element.style.marginBottom = '1px';
                  element.style.textDecoration = 'underline';
                  element.style.cursor = 'pointer';
                  element.addEventListener('click', () => {
                    this.search({ value: suggestion.titulo });
                  });
                  element.innerText = suggestion.titulo;
                  document
                    .getElementById('match-sugestion')
                    .insertAdjacentElement('beforeend', element);
                }
                contSuggestion += 1;
              }
            }
          });
        } else if (value === value?.toUpperCase()) {
          let completes = this.trends?.filter((trend: { assunto: string }) =>
            trend.assunto?.toUpperCase().startsWith(value?.toUpperCase()),
          );

          if (completes?.length === 0) {
            completes = this.trends?.filter((trend: { assunto: string }) =>
              this.removerSpecials(trend.assunto)
                ?.toLowerCase()
                .startsWith(value?.toLowerCase()),
            );
          }
          this.trend = completes[0]?.assunto?.toUpperCase();

          let suggestions = this.sugestoes?.filter(
            (suggestion: { titulo: string }) =>
              suggestion.titulo?.toLowerCase().includes(value?.toLowerCase()),
          );

          if (suggestions?.length === 0) {
            suggestions = this.sugestoes?.filter(
              (suggestion: { titulo: string }) =>
                this.removerSpecials(suggestion.titulo)
                  ?.toLocaleLowerCase()
                  ?.includes(value?.toLowerCase()),
            );
          }

          if (suggestions?.length > 0) {
            if (document.getElementById('match-sugestion')) {
              document.getElementById('match-sugestion').innerHTML = '';
              suggestions.forEach((value, index) => {
                if (index < 5) {
                  const element = document.createElement('p');
                  element.style.fontSize = '13px';
                  element.style.marginBottom = '1px';
                  element.style.textDecoration = 'underline';
                  element.style.cursor = 'pointer';
                  element.addEventListener('click', () => {
                    this.search({ value: value.titulo });
                  });
                  element.innerText = value.titulo;

                  document
                    .getElementById('match-sugestion')
                    .insertAdjacentElement('beforeend', element);

                  // if (index < completes.length - 1)
                  //   document
                  //     .getElementById('match-sugestion')
                  //     .insertAdjacentText('beforeend', ', ');
                }
              });
            }
          } else {
            if (document.getElementById('match-sugestion'))
              document.getElementById('match-sugestion').innerHTML = '';
          }
        } else {
          let completes = this.trends?.filter((trend: { assunto: string }) =>
            trend.assunto?.toLocaleLowerCase().startsWith(value?.toLowerCase()),
          );

          if (completes?.length === 0) {
            completes = this.trends?.filter((trend: { assunto: string }) =>
              this.removerSpecials(trend.assunto)
                ?.toLowerCase()
                .startsWith(value?.toLowerCase()),
            );
          }
          this.trend = completes[0]?.assunto?.toLocaleLowerCase();
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          this.trend = `${value}${this.trend?.substr(value.length) || ''}`;

          let suggestions = this.sugestoes?.filter(
            (suggestion: { titulo: string }) =>
              suggestion.titulo?.toLowerCase().includes(value?.toLowerCase()),
          );

          if (suggestions?.length === 0) {
            suggestions = this.sugestoes?.filter(
              (suggestion: { titulo: string }) =>
                this.removerSpecials(suggestion.titulo)
                  ?.toLocaleLowerCase()
                  ?.includes(value?.toLowerCase()),
            );
          }

          if (suggestions?.length > 0) {
            if (document.getElementById('match-sugestion')) {
              document.getElementById('match-sugestion').innerHTML = '';
              suggestions.forEach((value, index) => {
                if (index < 5) {
                  const element = document.createElement('p');
                  element.style.fontSize = '13px';
                  element.style.marginBottom = '1px';
                  element.style.textDecoration = 'underline';
                  element.style.cursor = 'pointer';
                  element.addEventListener('click', () => {
                    this.search({ value: value.titulo });
                  });
                  element.innerText = value.titulo;
                  document
                    .getElementById('match-sugestion')
                    .insertAdjacentElement('beforeend', element);
                  // if (index < completes.length - 1)
                  //   document
                  //     .getElementById('match-sugestion')
                  //     .insertAdjacentText('beforeend', ', ');
                }
              });
            }
          } else {
            if (document.getElementById('match-sugestion'))
              document.getElementById('match-sugestion').innerHTML = '';
          }
        }
      }
    } else {
      if (document.getElementById('match-sugestion'))
        document.getElementById('match-sugestion').innerHTML = '';
    }
  }

  /**
   * Press Tab prennche a barra de pesquisa com a sugestão.
   *
   * @param {any} event
   */
  onKey(event: KeyboardEvent): void {
    if (event.code === 'Tab') {
      event.preventDefault();
      if (this.trend) {
        document
          .querySelectorAll('#search')
          .forEach((searchInput) =>
            searchInput.setAttribute('value', this.trend),
          );
        this.trend = '';
      }
    } else {
      this.trend = '';
    }
  }

  ngOnDestroy(): void {
    this.trend = '';
    document
      .querySelectorAll('.autocomplete > span')
      ?.forEach((term) => (term.textContent = ''));
    document
      .querySelectorAll('#search')
      ?.forEach((searchInput) => searchInput.setAttribute('value', ''));

    if (document.getElementById('match-sugestion'))
      document.getElementById('match-sugestion').innerHTML = '';
  }
}
