/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ChatbotComponentComponent } from './chatbot-component.component';
import { LocalStorageService } from 'ngx-webstorage';

declare global {
  interface Window {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    LumaBot: any;
  }
}

describe('ChatbotComponentComponent', () => {
  let component: ChatbotComponentComponent;
  let fixture: ComponentFixture<ChatbotComponentComponent>;

  beforeEach(
    waitForAsync(() => {
      void TestBed.configureTestingModule({
        declarations: [ChatbotComponentComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        providers: [
          LocalStorageService,
          { provide: ComponentFixtureAutoDetect, useValue: true },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ChatbotComponentComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    void expect(component).toBeTruthy();
  });

  it('should open minerva chatbot', fakeAsync(() => {
    spyOn(component, 'toggleChatWidget');
    const minervaButton =
      fixture.debugElement.nativeElement.querySelector('.luma-chat-toggle');
    minervaButton.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    tick();
    expect(component.toggleChatWidget).toHaveBeenCalled();
  }));

  it('should toggle minerva chatbot', fakeAsync(() => {
    spyOn(window.LumaBot, 'toggleChatWidget');
    component.toggleChatWidget();
    fixture.detectChanges();
    tick();

    expect(window.LumaBot.toggleChatWidget).toHaveBeenCalled();
  }));
});
