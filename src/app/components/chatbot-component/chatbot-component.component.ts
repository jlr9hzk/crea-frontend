/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import { Component, OnInit } from '@angular/core';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare let LumaBot: any;

@Component({
  selector: 'app-chatbot-component',
  templateUrl: './chatbot-component.component.html',
  styleUrls: ['./chatbot-component.component.scss'],
})
export class ChatbotComponentComponent implements OnInit {
  /**
   * @ignore
   */
  constructor() {}

  /**
   * Carrega configuração de serviços LumaBot ao iniciar a pagina
   */
  ngOnInit(): void {
    LumaBot(
      {
        botName: 'Minerva',
        user: {
          clientUserId: '',
          additionalProperties: {},
        },
        secretkey:
          'sOQfST/muHYPez+7XU1BnJybljEArRg3MnHrQbGUOG4tYB+bCxUt5F6d84Ellnaxwd1lRbtW6IhwrM3Gc9q+YqZJ38+5O9z3IQ7QOpw5tdZQyaFQkfjVBdKJa5mbKt8XORi0u4+f0dUHzKcRLDyQ/Bx2pqxzjjvA8F/EUp9YeFFFy1NF/ZFqnusaR3fPTuoriGKtvWTzxPEKHvTiSEpNa3RJp1pcA3k1llMjf1I2WND1cKkVyRXZwguCFDEw6mGeDXnjvC23jTNKQJulFSp/xg==',
        domain: 'https://luma3.serviceaide.com/',
        styleOptions: {
          lumaChatContainer: {
            width: '400px',
          },
        },
      },
      document.getElementById('botHere'),
    );
  }

  /**
   * Abre o Chat Luma
   */
  toggleChatWidget(): void {
    LumaBot.toggleChatWidget();
  }
}
