/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * Prevents Angular change detection from
 * running with certain Web Component callbacks
 */
// eslint-disable-next-line no-underscore-dangle
// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
(window as any).__Zone_disable_customElements = true;
